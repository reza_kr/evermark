<?php

	session_start();

	require_once('class.phpmailer.php');


	Class Email { 

		public function __construct () {

			$this->emailHeader = '<table width="690" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" align="center" style="max-width:690px;padding-top:0px;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;background-color:#ffffff;border:1px solid #dddddd;">
							        <tbody><tr>
							            <td colspan="3" width="100%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f9f9f9" style="padding-top:0px;border-collapse:collapse;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;background-color:#F4F4F4;border-bottom:1px solid #EEE;">
							              <tbody><tr>
							                <td width="3%"></td>
							                <td width="94%" height="64"><a href="http://www.evermarkapp.com"><img src="http://www.evermarkapp.com/images/logo.png" alt="Evermark" title="Evermark" border="0" width="224" height="54" style="vertical-align:middle;display:block;"></a></td>
							                <td width="3%"></td>
							              </tr>
							            </tbody></table></td>
							        </tr>
							        <tr> 
							          <td colspan="3" width="100%" height="30"></td>
							        </tr>';

 			$this->emailFooter = '<tr>
							          <td colspan="3" width="100%" height="25"></td>
							        </tr>
							 
							      
							        <tr>
							          <td colspan="3" width="100%" height="25" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							        </tr>
							        <tr bgcolor="#E6E6E6" style="background-color:#E6E6E6;">

							            	<td width="3%" style="border-top:1px solid #dddddd;"></td>
							              <td width="94%" style="padding:20px 0; border-top:1px solid #dddddd;"><p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:10px;line-height:12px;color:#989898;">Copyright &copy; ' . date('Y') . ' Evermark. 
							              </td>
							              <td width="3%" style="border-top:1px solid #dddddd;"></td>
							        </tr>


							      </tbody>

							</table>';

			$this->mail = new PHPMailer;
			$this->mail->CharSet = 'UTF-8';
			$this->mail->isHTML(true);  
			$this->mail->WordWrap = 50;
			$this->mail->From = 'noreply@evermarkapp.com';
			$this->mail->FromName = 'Evermark'; 
			// $this->mail->isSMTP();                                      // Set mailer to use SMTP
			// $this->mail->Host = 'smtp1.example.com; smtp2.example.com';  // Specify main and backup server
			// $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
			// $this->mail->Username = 'jswan';                            // SMTP username
			// $this->mail->Password = 'secret';                           // SMTP password
			// $this->mail->SMTPSecure = 'ssl';                            // Enable encryption, 'ssl' also accepted

		}


		public function register($username, $email, $token) {

			$this->mail->addAddress($email);

			$this->mail->Subject = 'Evermark – Sign Up Confirmation';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Evermark – Sign Up Confirmation</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $username . ',</strong><br><br>We are delighted to have you on board!<br>To get you up and running, we just need to confirm that the email address you provided really does belong to you. Please follow the provided link below and you will be forwarded to the login page immediately.<br><br> Confirmation Link: <br><a style="word-break:break-all;" href="http://evermarkapp.com/app/signup_confirmation.php?token=' . $token . '&email=' . $email . '" target="_blank" >http://evermarkapp.com/app/signup_confirmation.php?token=' . $token . '&email=' . $email . '</a><br><br>Sincerely,<br><strong>The Evermark Team</strong><br><a href="http://evermarkapp.com">www.evermarkapp.com</a>


							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			
			} else {

				return true;
			}

		}


		public function confirmationReceived($email) {

			$this->mail->addAddress($email);

			$this->mail->Subject = 'Evermark – Welcome on Board!';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Evermark – Welcome on Board!</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Greetings from Evermark,</strong><br><br>Thank you for your sign up. We\'re confident that Evermark will be a helpful tool for managing your bookmarks!<br><br>Thank you for using Evermark and have a great day!<br><br>Sincerely,<br><strong>The Evermark Team</strong><br><a href="http://evermarkapp.com">www.evermarkapp.com</a>

							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			}

			return true;

		}



		// public function newUserInvitation($business_name, $first_name, $last_name, $email) {

		// 	$business_name = $_SESSION['business_name'];

		// 	$this->mail->addAddress($email, $first_name . ' ' . $last_name);

		// 	$this->mail->Subject = 'Iron Vault – ' . $business_name . ' has invited you to join the team!';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – ' . $business_name . ' has invited you to join the team!</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>You have been invited by ' . $business_name . ' to join their team on Iron Vault!<br>To accept this invitation and to gain access to their network, follow the link provided.<br><br>Join ' . $business_name . ': <br><a style="word-break:break-all;" href="https://' . $_SESSION['subdomain'] . '.ironvault.ca/join" target="_blank" >https://' . $_SESSION['subdomain'] . '.ironvault.ca/join</a><br><br>Thank you for using Iron Vault and have a great day!<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;				        


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;

		// }



		public function resetPassword($username, $email, $token) {

			$this->mail->addAddress($email);

			$this->mail->Subject = 'Evermark – Password Reset Request';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Evermark – Password Reset Request</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $username . ',</strong><br><br>We have received your password reset request.<br>To reset your password, please use the temporary link provided below. You have 24 hours to reset your password with the provided link. Thereafter you are required to request a new password reset.<br><br> Temporary Reset Link: <br><a style="word-break:break-all;" href="http://evermarkapp.com/app/password_change.php?token=' . $token . '&email=' . $email . '" target="_blank" >http://evermarkapp.com/password_change.php?token=' . $token . '&email=' . $email . '</a><br><br>Thank you for using Evermark and have a great day!<br><br>Sincerely,<br><strong>The Evermark Team</strong><br><a href="http://evermarkapp.com">www.evermarkapp.com</a>


							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			}

			return true;
		}


		// public function passwordResetBySupervisor($first_name, $last_name, $email, $token) {

		// 	$this->mail->addAddress($email, $first_name . ' ' . $last_name);

		// 	$this->mail->Subject = 'Iron Vault – Password Reset Request by Supervisor';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – Password Reset Request by Supervisor</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>Your supervisor has requested for your password to be reset.<br>To reset your password, please use the temporary link provided below. You have 24 hours to reset your password with the provided link. Thereafter you are required to request a new password reset.<br><br> Temporary Reset Link: <br><a style="word-break:break-all;" href="https://' . $_SESSION['subdomain'] . '.ironvault.ca/password_change?token=' . $token . '&email=' . $email . '" target="_blank" >https://' . $_SESSION['subdomain'] . '.ironvault.ca/password_change?token=' . $token . '&email=' . $email . '</a><br><br>If you did not expect a password reset and/or suspect that your account has been compromised with, we advice you to get in touch with us immediately at <a href="mailto:security@ironvault.ca">security@ironvault.ca</a>.<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;				        


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;
		// }


		public function confirmPasswordReset($username, $email) {

			$this->mail->addAddress($email);

			$this->mail->Subject = 'Evermark – Password Change Confirmation';

			$this->mail->Body   .= $this->emailHeader;	

			$this->mail->Body    .=  '<tr>
							          <td width="3%"></td>
							          <td width="94%">
							            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                <tbody>
							                    <tr>
							                      <td width="60%" valign="top">

							                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
							                                <tbody>

							                                  <tr>
							                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Evermark – Password Change Confirmation</td>
							                                  </tr>
							    
							                                </tbody>
							                          </table>

							                      </td>
							                    </tr>
							                    <tr>
							                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
							                    </tr>

							                </tbody>
							            </table>

							          </td>  
							          <td width="3%"></td>
							        </tr>'; 

			$this->mail->Body   .= '<tr>
							            <td colspan="3" width="100%" height="25"></td>
							        </tr>

							        <tr>
							        	 <td width="3%"></td>

							          	  <td width="94%">
							                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

							                  	<strong>Dear ' . $username . ',</strong><br><br>Your password has been successfully updated.<br><br>Thank you for using Evermark and have a great day!<br><br>Sincerely,<br><strong>The Evermark Team</strong><br><a href="http://evermarkapp.com">www.evermarkapp.com</a>


							                	</p>
							              </td>

							        	   <td width="3%"></td>
							        </tr>';

			$this->mail->Body   .= $this->emailFooter;				        


			if(!$this->mail->send()) {

			   return false;
			}

			return true;
		}


		// public function accountNotFound($email) {

		// 	$this->mail->addAddress($email);

		// 	$this->mail->Subject = 'Iron Vault – No account found, Sign Up instead?';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – No account found, Sign Up instead?</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Greetings from Iron Vault,</strong><br><br>We were unable to find an account associated with this email address. Could it be that you don\'t have an account with us yet?<br><br>If that\'s the case, then we would like to welcome you to join us!<br>Visit our website at <a href"http://www.ironvault.ca">http://www.ironvault.ca</a> to join. It takes less than a minute!<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;

		// }


		// public function userAccountDisabled($first_name, $last_name, $email) {

		// 	$this->mail->addAddress($email);

		// 	$this->mail->Subject = 'Iron Vault – User Account Disabled by Supervisor';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – User Account Disabled by Supervisor‏</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>Your supervisor has disabled your account from accessing the company\'s portal. Please understand that we have no control over this and advice you to contact your supervisor for further details. We hope to have you back soon!<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;

		// }


		// public function billingStatement($business_name, $subdomain, $email, $first_name, $last_name, $total) {

		// 	$month = date('F');

		// 	$this->mail->addAddress($email);

		// 	$this->mail->Subject = 'Iron Vault – ' . $month . ' Billing Statement';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – ' . $month . ' Billing Statement</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Greetings from Iron Vault,</strong><br><br>We would like to inform you that your billing statement is available. Your account will be charged the following amount:<br><br>Total: $' . $total . '<br><br>To see a complete breakdown of all the charges please log on to your account and view the Plan & Payments tab on the Account Settings page.<br><br>Thank you for using Iron Vault and have a great day!<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;

		// } 



		// public function accountChangesMade($first_name, $last_name, $email, $account_changes) {

		// 	$month = date('F');

		// 	$this->mail->addAddress($email);

		// 	$this->mail->Subject = 'Iron Vault – Confirmation of Changes to Your Account';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – Confirmation of Changes to Your Account</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>This email is to confirm that the following changes have been made to your account:<br><br><ul> ' . $account_changes . ' </ul><br><br>If you have no knowledge of these changes and/or suspect that your account has been compromised with, we advice you to get in touch with us immediately at <a href="mailto:security@ironvault.ca">security@ironvault.ca</a>.<br><br>Thank you for using Iron Vault and have a great day!<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;

		// } 


		// public function pendingAccountDeletion($first_name, $last_name, $email) {

		// 	$this->mail->addAddress($email);

		// 	$this->mail->Subject = 'Iron Vault – Pending Account Deletion by Supervisor';

		// 	$this->mail->Body   .= $this->emailHeader;	

		// 	$this->mail->Body    .=  '<tr>
		// 					          <td width="3%"></td>
		// 					          <td width="94%">
		// 					            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                <tbody>
		// 					                    <tr>
		// 					                      <td width="60%" valign="top">

		// 					                          <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" style="font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;border-collapse:collapse;">
		// 					                                <tbody>

		// 					                                  <tr>
		// 					                                      <td width="100%" style="font-size:28px;line-height:36px;color:#434343;">Iron Vault – Pending Account Deletion by Supervisor‏</td>
		// 					                                  </tr>
							    
		// 					                                </tbody>
		// 					                          </table>

		// 					                      </td>
		// 					                    </tr>
		// 					                    <tr>
		// 					                    	   <td colspan="2" width="100%" height="10" style="border-collapse:collapse;border-bottom:1px solid #f1f1f1;"></td>
		// 					                    </tr>

		// 					                </tbody>
		// 					            </table>

		// 					          </td>  
		// 					          <td width="3%"></td>
		// 					        </tr>'; 

		// 	$this->mail->Body   .= '<tr>
		// 					            <td colspan="3" width="100%" height="25"></td>
		// 					        </tr>

		// 					        <tr>
		// 					        	 <td width="3%"></td>

		// 					          	  <td width="94%">
		// 					                  	<p style="padding:0;font-family:Arial, Helvetica, sans-serif;font-weight:normal;font-size:13px;line-height:21px;color:#434343;">

		// 					                  	<strong>Dear ' . $first_name . ' ' . $last_name . ',</strong><br><br>Your supervisor has requested for your account to be permanently removed. If the account deletion is not reverted within the next 7 days, your account will be removed from the system along with any files kept in your personal Storage. Please understand that we have no control over this and advice you to contact your supervisor if you have further questions. We hope to have you back again in the future!<br><br>Sincerely,<br><strong>The Iron Vault Team</strong><br><a href="http://www.ironvault.ca">www.ironvault.ca</a>


		// 					                	</p>
		// 					              </td>

		// 					        	   <td width="3%"></td>
		// 					        </tr>';

		// 	$this->mail->Body   .= $this->emailFooter;


		// 	if(!$this->mail->send()) {

		// 	   return false;
		// 	}

		// 	return true;

		// }

	}

?>