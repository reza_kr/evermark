<?php

	header('Access-Control-Allow-Origin: *');

	session_start();

	require_once("classes/Mobile.class.php");
	$mobile = new Mobile();

	// ===============================================
	//These are actions which do no require the user to be logged in....
	if($_POST['action'] == "check_username") {

		$result = $mobile->checkUsername();

		echo $result;
	
	} else if($_POST['action'] == "check_session") {

		$result = $mobile->checkSession();

		echo $result;
	
	} else if($_POST['action'] == "login") {

		$result = $mobile->login();

		echo $result;
	}

	// ==============================================


	// ==============================================
	// These are actions that require the user to be logged in...

	$session = $mobile->checkSession();

	if($session == 'active') {

		if($_POST['action'] == "load_bookmarks") {

			$result = $mobile->loadBookmarks();

			echo $result;

		} else if($_POST['action'] == "add_bookmark") {

			$result = $mobile->addBookmark();

			echo $result;

		} else if($_POST['action'] == "update_bookmark") {

			$result = $mobile->updateBookmark();

			echo $result;

		} else if($_POST['action'] == "delete_bookmark") {

			$result = $mobile->deleteBookmark();

			echo $result;

		} else if($_POST['action'] == "delete_category") {

			$result = $mobile->deleteCategory();

			echo $result;
		
		} else if($_POST['action'] == "add_category") {

			$result = $mobile->addCategory();

			echo $result;
		
		} else if($_POST['action'] == "get_cat_options") {

			$result = $mobile->getCatOptions();

			echo $result;

		} else if($_POST['action'] == "load_categories") {

			$result = $mobile->loadCategories();

			echo $result;

		} else if($_POST['action'] == "load_new_bookmark_info") {

			$result = $mobile->getStaticCopy();

			echo $result;

		} else if($_POST['action'] == "sign_out") {

			$result = $mobile->logout();

			echo $result;
		}

	} else {

		echo 'inactive';
	}
?>