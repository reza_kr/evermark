<!DOCTYPE html>
<html lang="en" class="">
    <head>
        <meta charset="utf-8" />
        <meta name="google-site-verification" content="gwcy3rD4RypWoCpgzEOETxNrUJuU8iTaOlbpPsNx5gE" />
        <title>Evermark | Bookmarks That Last.</title>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
        <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
        <link rel="stylesheet" href="css/landing.css" type="text/css" cache="false" />
        <link rel="stylesheet" href="css/custom.css" type="text/css" cache="false" />
        <!--[if lt IE 9]> <script src="js/ie/html5shiv.js" cache="false"></script> <script src="js/ie/respond.min.js" cache="false"></script> <script src="js/ie/excanvas.js" cache="false"></script> <![endif]-->
    
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"> 
    </head>
    <body>
        <!-- header --> 
        <header id="header" class="navbar navbar-fixed-top bg-white box-shadow b-b b-light" data-spy="affix" data-offset-top="1">
            <div class="container">
                <div class="navbar-header"> <a href="#" class="navbar-brand"><img src="images/logo.png" class="m-r-sm"></a> <button class="btn btn-link visible-xs" type="button" data-toggle="collapse" data-target=".navbar-collapse"> <i class="fa fa-bars"></i> </button> </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li> <a href="app/login.php" class="btn btn-sm m-l">Sign In</a> </li>
                        <li> <a href="app/signup.php" class="btn btn-sm btn-success m-l">Sign Up</a> </li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- / header -->
        <section id="content">
            <div class="bg-primary dk">
                <div class="text-center wrapper">
                    <div class="m-t-xl m-b-xl">
                        <div class="text-uc h1 font-bold inline">
                            <div class="pull-left m-r-sm text-white">All Your Bookmarks <span class="font-thin">In One Place.</span></div>
                            <div class="carousel slide carousel-fade inline auto aside text-left pull-left pull-none-xs" data-interval="2000">
                                <div class="carousel-inner">
                                    <div class="item active text-dark">Desktop</div>
                                    <div class="item text-dark">Tablet</div>
                                    <div class="item text-dark">Mobile</div>
                                </div>
                            </div>
                        </div>
                        <div class="h2 m-t-sm font-thin">Accessible through the browser, as an extension*, or the mobile app*!</div>
                        <div class="m-t-sm font-thin">* -Browser Extensions and Android/iOS apps coming soon!</div>
                    </div>
                    <p class="text-center m-b-l"> <a href="app/login.php" class="btn btn-lg btn-dark m-sm xl-width">Sign In</a> <a href="app/signup.php" class="btn btn-lg btn-dark m-sm xl-width">Sign Up</a></p>
                </div>

                <div class="padder">
                    <div class="hbox">

                        <aside class="col-md-3 v-bottom text-right"><div class="hidden-sm hidden-xs"></div></aside>

                        <aside class="col-sm-10 col-md-6">
                            <section class="panel bg-dark m-b-n-lg no-border device animated fadeInUp"> 
                                <header class="panel-heading text-left"> 
                                    <i class="fa fa-circle fa-fw"></i> 
                                    <i class="fa fa-circle fa-fw"></i> 
                                    <i class="fa fa-circle fa-fw"></i> 
                                </header> 
                                <img src="images/evermark_screenshot.png" class="img-full"> 
                            </section> 
                        </aside>

                        <aside class="col-md-3 v-bottom text-right"><div class="hidden-sm hidden-xs"></div></aside>
                    </div>
                </div>

            </div>
            <div id="about">
                <div class="container">
                    <div class="m-t-xl m-b-xl text-center wrapper">
                        <h2>It's Time To Make Your Bookmarks Go Platform-Independent.</h2>
                        <h4>Avoid the disorganized mess that is your current bookmarks collection. Evermark is your way out.</h4>
                    </div>
                    <div class="row m-t-xl m-b-xl text-center">
                        <div class="col-sm-4">
                            <p class="h3 m-b-lg"> <i class="fa fa-lightbulb-o fa-3x text-info"></i> </p>
                            <div class="">
                                <h3 class="m-t-none">Intuitive Interface</h3>
                                <p class="m-t-lg">Our focus is simplicity and efficiency. With our simple, yet well thought-out interface, you are able to access what you need - when you need it!</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <p class="h3 m-b-lg"> <i class="fa fa-cloud fa-3x text-info"></i> </p>
                            <div class="">
                                <h3 class="m-t-none">Cloud-Hosted Static Copies</h3>
                                <p class="m-t-lg">Our solution to dead bookmark links! Never be worried about taken down or modified bookmarks again! Evermark allows you to seemlessly choose between the live bookmark or a static copy to view.</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <p class="h3 m-b-lg"> <i class="fa fa-search fa-3x text-info"></i> </p>
                            <div class="">
                                <h3 class="m-t-none">Instant Search &amp; Filter</h3>
                                <p class="m-t-lg">Our instant search and filtering allow you to quickly access what you're looking for. Type a term in the search bar or use categories to organize your bookmarks!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- footer --> 
        <footer id="footer">
            <div class="bg-primary text-center">
                <div class="container wrapper">
                    <div class="h3 m-t-xl m-b"> The Best Part – It's Completely FREE! <a href="app/signup.php" class="btn btn-xl btn-dark b-white bg-empty m-sm">Sign Up Now</a></div>
                </div>
                <i class="fa fa-caret-down fa-4x text-primary m-b-n-lg block"></i> 
            </div>
            <div class="bg-dark dker wrapper">
                <div class="container text-center m-t-lg">
                    <div class="row m-t-xl m-b-xl">
                        <div class="col-sm-12" data-ride="animated" data-animation="fadeInUp" data-delay="300">
                            <i class="fa fa-envelope-o fa-3x icon-muted"></i> 
                            <h5 class="text-uc m-b m-t-lg">Have Questions or Suggestions? EMail Us</h5>
                            <p class="text-sm"><a href="mailto:info@evermarkapp.com">info@evermarkapp.com</a></p>
                        </div>
                    </div>
                    <div class="m-t-xl m-b-xl">
                        <p> <a href="#content" data-jump="true" class="btn btn-icon btn-rounded btn-dark b-dark bg-empty m-sm text-muted"><i class="fa fa-angle-up"></i></a> </p>
                    </div>
                </div>
            </div>
        </footer>
        <!-- / footer --> <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> <script src="js/appear/jquery.appear.js" cache="false"></script> <script src="js/scroll/smoothscroll.js" cache="false"></script> <script src="js/landing.js" cache="false"></script>
    </body>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-54531829-1', 'auto');
      ga('send', 'pageview');

    </script>
</html>