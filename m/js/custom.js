$(document).ready(function() {

	var sess_id = localStorage.getItem('sess_id');

	$.ajax({

        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
        type: 'POST',
        crossDomain: true,
        data: {
        	action: "check_session",
        	session_id: sess_id
        }

    }).done(function(data) { alert(data);

		if(data == 'inactive') {

			window.location.href = 'login.html';
		}

		data = '';

	});


	$('nav#menu').mmenu();


	// //Isotope init

	setTimeout(function() {
		// quick search regex
	  	var qsRegex;

		$(".isotope").isotope({
		  itemSelector: '.panel',
		  layoutMode: 'fitRows',
		  onLayout: function() {
            $(window).trigger("scroll");
          },
		  filter: function() {
		    return qsRegex ? $(this).find(".panel-heading, .panel-body").text().match( qsRegex ) : true;
		  }
		}).css('overflow','auto');

		// use value of search field to filter
		var $quicksearch = $('#search_box').keyup(debounce(function() {

			$('#menu ul li').removeClass('active');
			$('#menu ul li#show_all').addClass('active');

		    qsRegex = new RegExp( $quicksearch.val(), 'gi' );
		    $(".isotope").isotope({
		    	filter: function() {
				    return qsRegex ? $(this).find(".panel-heading, .panel-body").text().match( qsRegex ) : true;
				}
		    });

		    setTimeout(function() {

			    if($(".isotope .panel:visible").length == 0) {

			    	$(".no_results").fadeIn(240);
			   
			    } else {

			   		$(".no_results").fadeOut(240);
			    }

			}, 500);

		}, 200 ));


	}, 250);

	// debounce so filtering doesn't happen every millisecond
	function debounce( fn, threshold ) {
	  var timeout;
	  return function debounced() {
	    if ( timeout ) {
	      clearTimeout( timeout );
	    }
	    function delayed() {
	      fn();
	      timeout = null;
	    }
	    timeout = setTimeout( delayed, threshold || 100 );
	  }
	}


	// -----------------

	//Load Bookmarks
	$.ajax({
        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
        type: 'POST',
        crossDomain: true,
        data: {
        	action: "load_bookmarks",
        	session_id: sess_id
        }

    }).done(function(data) {

		$('.isotope').html(data);

		data = '';

	});


	//Lazy Load

	$(".isotope .panel img").lazyload({
	    threshold : 100,
	    effect : "fadeIn"
	});

	$('body').scroll(function() {

		$(window).resize();

	});

	// -----------------


	// Load categories
	$.ajax({

        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
        type: 'POST',
        data: {
        	action: "load_categories",
        	session_id: sess_id
        }

    }).done(function(data) {

		$('#menu ul').append(data);

		data = '';

	});


	//Get categories options dropdowm // for add bookmark.
	$.ajax({

        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
        type: 'POST',
        data: {
        	action: "get_cat_options",
        	session_id: sess_id
        }

    }).done(function(data) {

		$('.add_bookmark_wrapper #category').html(data);

		data = '';

	});


	// $('body').on('click', '.static_copy', function() {

	// 	var bookmark_title = $(this).closest(".panel").find('.bookmark_title').text();
	// 	var static_url = $(this).attr("href");
	// 	var date = $(this).attr("data-date");

	// 	if(static_url != "") {
	// 		load_static_viewer(bookmark_title, static_url, date);
	// 	}

	// 	return false;

	// });


	// function load_static_viewer(bookmark_title, static_url, date) {

	// 	$('.add_bookmark_wrapper').hide();

	// 	var height = $(window).height();

	// 	$('.isotope').fadeOut(500);

	// 	$('.static_viewer').show();

	// 	$('.static_viewer').animate({
	// 		height: height - 75
	// 	});

	// 	$('iframe#static_viewer_frame').animate({
	// 		height: height - 115
	// 	}, function() {

	// 		$('.static_viewer .static_viewer_header .page_title').text(bookmark_title + ' | Static Copy (' + date + ')');

	// 		$('iframe#static_viewer_frame').attr("src", static_url);
	// 	});

	// }


	// $('.close_static_viewer').click(function() {

	// 	$('.isotope').fadeIn('fast', function() {
	// 		$('.isotope').isotope();
	// 		$('.add_bookmark_wrapper').fadeIn('fast');
	// 	});

	// 	$('.static_viewer').animate({
	// 		height: 0
	// 	}, function() {

	// 		$('.static_viewer').fadeOut('fast');
	// 	});

	// 	$('iframe#static_viewer_frame').animate({
	// 		height: 0
	// 	});

	// 	$('iframe#static_viewer_frame').attr("src", "");

	// 	return false;
	// });


	$('#menu #sign_out').click(function() {

		$.ajax({

	        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "sign_out"
	        }

	    }).done(function(data) {

			if(data == 'success') {

				window.location.href = 'login.html';
			}

			data = '';

		});

		return false;

	});



	$('#menu #add_bookmark').click(function() {

		$.ajax({

	        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "get_cat_options",
	        	session_id: sess_id
	        }

	    }).done(function(data) {

			$('.add_bookmark_wrapper #category').html(data);

			data = '';

		});

		return false;
	});


	$('.add_bookmark_wrapper #save_bookmark').click(function() {

		add_bookmark();

		return false;
	});

	$('.add_bookmark_wrapper form').submit(function() {

		add_bookmark();

		return false;
	});


	function add_bookmark() {

		var bookmark_title = $('.add_bookmark_wrapper #bookmark_title').val();
		var bookmark_url = $('.add_bookmark_wrapper #bookmark_url').val();
		var bookmark_cat = $('.add_bookmark_wrapper #category option:selected').val();

		bookmark_title = bookmark_title.trim();
		bookmark_url = bookmark_url.trim();

		if(bookmark_url != '') {

			$('.add_bookmark_wrapper #bookmark_title').prop('disabled', true);
			$('.add_bookmark_wrapper #bookmark_url').prop('disabled', true);
			$('.add_bookmark_wrapper #category').prop('disabled', true);
			$('.add_bookmark_wrapper #save_bookmark').prop('disabled', true);
			$('.add_bookmark_wrapper .spinner').show();

			$('.isotope .welcome').fadeOut('fast');

			$.ajax({

		        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "add_bookmark",
		        	bookmark_title: bookmark_title,
		        	bookmark_url: bookmark_url,
		        	bookmark_cat: bookmark_cat,
		        	session_id: sess_id
		        }

		    }).done(function(data) {

		    	if(data == 'not_allowed') {

		    		$('.add_bookmark_wrapper .spinner').hide();

					$('.add_bookmark_wrapper #bookmark_title').prop('disabled', false);
					$('.add_bookmark_wrapper #bookmark_url').prop('disabled', false);
					$('.add_bookmark_wrapper #category').prop('disabled', false);
					$('.add_bookmark_wrapper #save_bookmark').prop('disabled', false);

					$('#not_allowed').fadeIn('fast');
					setTimeout(function() {
						$('#not_allowed').fadeOut('fast');
					}, 2500);

		    	} else if(data == 'url_unreachable') {

		    		$('.add_bookmark_wrapper .spinner').hide();

					$('.add_bookmark_wrapper #bookmark_title').prop('disabled', false);
					$('.add_bookmark_wrapper #bookmark_url').prop('disabled', false);
					$('.add_bookmark_wrapper #category').prop('disabled', false);
					$('.add_bookmark_wrapper #save_bookmark').prop('disabled', false);

					$('#url_unreachable').fadeIn('fast');
					setTimeout(function() {
						$('#url_unreachable').fadeOut('fast');
					}, 2500);

		    	} else {	

					$('.add_bookmark_wrapper #bookmark_title').val('');
					$('.add_bookmark_wrapper #bookmark_url').val('');
					$('.add_bookmark_wrapper #category').val('');

					$('.add_bookmark_wrapper .spinner').hide();

					$('.add_bookmark_wrapper #bookmark_title').prop('disabled', false);
					$('.add_bookmark_wrapper #bookmark_url').prop('disabled', false);
					$('.add_bookmark_wrapper #category').prop('disabled', false);
					$('.add_bookmark_wrapper #save_bookmark').prop('disabled', false);

					$(".isotope").prepend(data).isotope('reloadItems').isotope();

					bookmark_id = $('.isotope .panel:first-child').attr('id');

					$.ajax({

				        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
				        type: 'POST',
				        timeout: 8000,
				        data: {
				        	action: "load_new_bookmark_info",
				        	bookmark_id: bookmark_id,
				        	session_id: sess_id
				        },
				        error: function() { 

					    	$('.isotope .panel#' + bookmark_id + ' .panel-body img').attr('src', 'images/placeholder.png');

					    	$('.isotope .panel#' + bookmark_id + ' .loader').remove();

				        },
				        success: function(data) {

				        	data_args = data.split(',');

					    	bookmark_img = data_args[0];
					    	bookmark_static_url = data_args[1];

					    	$('.isotope .panel#' + bookmark_id + ' .panel-body img').attr('src', bookmark_img);
					    	$('.isotope .panel#' + bookmark_id + ' .panel-footer .static_copy').attr('href', bookmark_static_url);

					    	if(bookmark_static_url != '') {
					    		$('.isotope .panel#' + bookmark_id + ' .panel-footer .static_copy').removeClass('disabled');
					    	}

					    	$('.isotope .panel#' + bookmark_id + ' .loader').remove();
				        }

				    });

				}

				data = '';

			});
		}
	} 


	$('body').on('click', '.delete_bookmark', function() {

		var bookmark_id = $(this).closest('section').attr('id');

		$.ajax({

	        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "delete_bookmark",
	        	bookmark_id: bookmark_id,
	        	session_id: sess_id
	        }

	    }).done(function(data) {

			$('.isotope section#' + bookmark_id).fadeOut(240, function() {

				$(".isotope").isotope('remove', $(this));

				$(".isotope").isotope('layout');
			});

			data = '';

		});

		return false;

	});



	$('.add_category_wrapper #add_category').click(function() {

		add_category();

		return false;
	});

	$('.add_category_wrapper form').submit(function() {

		add_category();

		return false;
	});


	function add_category() {

		var cat_name = $('.add_category_wrapper #cat_name').val();

		$.ajax({

	        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "add_category",
	        	cat_name: cat_name,
	        	session_id: sess_id
	        }

	    }).done(function(data) {

			if(data != "failed") {

				$("#menu ul").append(data);
			}

			$('.add_category_wrapper #cat_name').val("");
			//$('.add_category_wrapper').slideUp(240);

			//RELOAD CATEGORIES DROPDOWN
			$.ajax({

		        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_cat_options",
		        	session_id: sess_id
		        }

		    }).done(function(data) {

				$('.add_bookmark_wrapper #category').html(data);

			});

			data = '';

		});

	}


	$('body').on('click', '.delete_category', function() {


		///ADD CONFIRM DIALOG BOX -- "Delete Category Name?"

		var li = $(this).closest('li');
		var li_cat_id = $(li).attr('id');

		var cat_id_parts = li_cat_id.split('-');
		cat_id = cat_id_parts[1];

		$.ajax({

	        url: 'http://evermarkapp.com/mobile/ajax_functions_mobile.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "delete_category",
	        	cat_id: cat_id,
	        	session_id: sess_id
	        }

	    }).done(function(data) {

			if(data == "success") {

				if($(li).is('.active')) {

					$('#menu ul li#show_all').trigger('click');
				}
			
				$('#menu ul li#' + li_cat_id).slideUp(240, function() {

					$(this).remove();
				});
			}

			data = '';

		});

		return false;
	});


	$('body').on('click', '#menu ul li > a', function() {

		$('#menu ul li').removeClass('active');
		$(this).closest('li').addClass('active');

		var id = $(this).closest('li').attr('id');

		if(id != '' && typeof id != 'undefined') {

			id = '.' + id;

			$('.isotope').isotope({ filter: id });

		} else {

			$('.isotope').isotope({ filter: '*' });
		}

		$('#search_box').val('');
		$("#search_box").animate({
			width: 0,
			duration: 250
		});

		return false;
	});

});