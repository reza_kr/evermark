
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
   <meta charset="utf-8" />

   <title>403 - Forbidden | Iron Vault</title>

   <meta content="width=device-width, initial-scale=1.0" name="viewport" />

   <meta content="" name="description" />

   <meta content="" name="author" />

   <link href="app/css/stylesheet.min.css" rel="stylesheet" type="text/css"/>
   <link href="css/error.css" rel="stylesheet" type="text/css"/>

   <link rel='shortcut icon' type='image/x-icon' href='img/favicon.ico' />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-404-3">
   <div class="page-inner">
      <img src="img/earth.jpg" alt="">
   </div>
   <div class="container error-404">
      <h1>403</h1>
      <h2>Forbidden</h2>
      <p>
         Astronaunt training is required to grant you access to this section.<br>
      </p>
      <p>
         <a href="https://www.ironvault.ca" class="light_blue">Return Home</a>
         <br>
      </p>
   </div>
</body>
<!-- END BODY -->
</html>