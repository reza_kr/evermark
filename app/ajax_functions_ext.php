<?php

	// ini_set('display_errors', 1); 
	// error_reporting(E_ALL);

	if($_POST['action'] == "check_session") {

		require_once("classes/Chrome.class.php");

		$chrome = new Chrome();

		$result = $chrome->checkSession();

		echo $result;
	}

	if(isset($_COOKIE['evermark_chrome_ext']) && $_COOKIE['evermark_chrome_ext'] == 'active') {

		if($_POST['action'] == "save_bookmark") {

			require_once("classes/Chrome.class.php");

			$chrome = new Chrome();

			$result = $chrome->saveBookmark();

			echo $result;

		} else if($_POST['action'] == "load_bookmarks") {

			require_once("classes/Chrome.class.php");

			$chrome = new Chrome();

			$result = $chrome->loadBookmarks();

			echo $result;
		
		} else if($_POST['action'] == "load_categories") {

			require_once("classes/Chrome.class.php");

			$chrome = new Chrome();

			$result = $chrome->loadCategories();

			echo $result;
		
		} else if($_POST['action'] == "sign_out") {

			require_once("classes/Chrome.class.php");

			$chrome = new Chrome();

			$result = $chrome->signOut();
			
		}

	} else {

		if($_POST['action'] == "login") { 

			require_once("classes/Chrome.class.php");

			$chrome = new Chrome();

			$result = $chrome->login();

			echo $result;

		}

	}


?>