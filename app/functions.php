<?php

	// ini_set('display_errors', 1); 
	// error_reporting(E_ALL);

	session_start(); 

	$page = basename($_SERVER['PHP_SELF']);

	if($_SESSION['status'] == "active") {


	} else {

		if($page != "login.php" && $page != "logout.php" && $page != "forgot_password.php" && $page != "password_change.php" && $page != "signup.php" && $page != "signup_confirmation.php") {

			header('Location: login.php');
		}
	}


	// if($_GET['action'] == 'put') {

	// 	require_once("classes/S3.class.php");
	// 	$s3 = new S3();

	// 	$s3->putObject();
	// }


	if($_GET['action'] == 'signup') {

		require_once("classes/Main.class.php");
		$main = new Main();

		$main->register();
	}


	function logout($msg) {
		require_once("classes/Main.class.php");
		$main = new Main();

		$main->logout($msg);
	}



	function load_page_html() {

		$page = basename($_SERVER['PHP_SELF']);


		require_once("classes/Main.class.php");
		$main = new Main();

		$html_array = array();

		if($page == "index.php") {

			header('Location: dashboard.php');

		} else if($page == "dashboard.php") {

			$html_array['cats'] = $main->loadCategories();

			$html_array['bookmarks'] = $main->loadBookmarks();

		} else if($page == "login.php") {


			if($_POST['username'] && $_POST['password']) {

				$main->login();
			}

			if($_SESSION['status'] == "active") {
				header('Location: dashboard.php');
			}
		
		} else if($page == "signup_confirmation.php") { 

			if($_GET['token'] && $_GET['email']) {

				$main->signupConfirmation();
			}

			if($_GET['e'] == 'verification_failed') {

				header('Location: login.php?e=verification_failed');
			
			} else if($_GET['m'] == 'verified') {

				header('Location: login.php?m=verified');
			}
		
		} else if($page == "forgot_password.php") {

			if($_GET['action'] == 'reset_password') {

				$main->resetPassword();
			}

		}  else if($page == "password_change.php") {

			if($_GET['token'] && $_GET['email']) {

				$main->checkPasswordResetToken();
			}

			if($_GET['action'] == 'change_password') {

				$main->changePassword();
			}

		}


		return $html_array;

	}

?>