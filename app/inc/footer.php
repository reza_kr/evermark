<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/app.v2.js"></script>
<script src="js/isotope.min.js"></script>
<!-- <script src="js/infinitescroll.min.js"></script> -->
<script src="js/jquery.lazyload.min.js"></script>
<script src="js/custom.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54531829-1', 'auto');
  ga('send', 'pageview');

</script>