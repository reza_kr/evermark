 	<head>
        <meta charset="utf-8" />
        <title>Evermark | Bookmarks That Last.</title>
        <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="stylesheet" href="css/app.v2.css" type="text/css" />
        <link rel="stylesheet" href="css/font.css" type="text/css" cache="false" />
        <link rel="stylesheet" href="css/custom.css" type="text/css" cache="false" />
        <!--[if lt IE 9]> 
        <script src="js/ie/html5shiv.js" cache="false"></script>
        <script src="js/ie/respond.min.js" cache="false"></script>
        <script src="js/ie/excanvas.js" cache="false"></script> 
        <![endif]-->

        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"> 
    </head>