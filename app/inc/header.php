            <header class="bg-dark dk header navbar navbar-fixed-top-xs">
                <div class="navbar-header aside-md"> <a class="btn btn-link visible-xs" data-toggle="class:nav-off-screen" data-target="#nav"> <i class="fa fa-bars"></i> </a> <a href="#" class="navbar-brand" data-toggle="fullscreen"><img src="images/logo_light.png" class="m-r-sm header_logo"></a> <a class="btn btn-link visible-xs" data-toggle="dropdown" data-target=".nav-user"> <i class="fa fa-cog"></i> </a> </div>

                <ul class="nav navbar-nav navbar-right hidden-xs nav-user">
                    <li>
                        <form role="search">
                            <input type="text" id="search_box" placeholder="Search..." autocomplete="off" />
                        </form>
                    </li>
                    <li>
                        <a href="#" class="search_box_toggle"><i class="fa fa-fw fa-search"></i></a> 
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="thumb-sm pull-left"> <i class="fa fa-user"></i> </span> <?php echo $_SESSION['email']; ?> <b class="caret"></b> </a> 
                        <ul class="dropdown-menu animated fadeInRight">
                            <span class="arrow top"></span> 
                            <!-- <li> <a href="#"><i class="fa fa-cog"></i>&nbsp;&nbsp;Settings</a> </li>
                            <li> <a href="help.php"><i class="fa fa-question"></i>&nbsp;&nbsp;&nbsp;Help</a> </li>
                            <li class="divider"></li> -->
                            <li> <a href="logout.php"><i class="fa fa-sign-out"></i>&nbsp;&nbsp;Logout</a> </li>
                        </ul>
                    </li>
                </ul>
            </header>