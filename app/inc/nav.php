                    <aside class="bg-dark lter aside-md hidden-print" id="nav">
                        <section class="vbox">
                            <!-- <header class="header bg-primary lter text-center clearfix">
                                <button type="button" class="btn btn-primary add_bookmark"><i class="fa fa-plus"></i> Add Bookmark</button> 
                            </header> -->
                            <section class="w-f scrollable">
                                <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="5px" data-color="#333333">
                                    <!-- nav --> 
                                    <nav class="nav-primary">
                                        <h4 class="m-l m-r">Categories</h4>

                                        <ul class="nav categories">
                                            <li class="active"> <a href="#"><i class="fa fa-chevron-right icon"> <b class="bg-danger"></b> </i> <span>Show All</span> </a> </li>
                                            <?php echo $page_html['cats']; ?>

                                            <li> <a href="#" id="open_add_category"><i class="fa fa-plus icon"> <b class="bg-danger"></b> </i> <span>Add New...</span> </a> </li>
                                        </ul>

                                        <div class="add_category_wrapper m-t">
                                            <form method="post" action="#">
                                                <input type="text" id="cat_name" placeholder="Category Name" />
                                                <button type="button" id="add_category" class="btn btn-primary add_category m-l m-r"><i class="fa fa-plus"></i> Add Category</button>
                                            </form>
                                        </div>
                                    </nav>
                                    <!-- / nav --> 

                                    <!-- Appstr.io skyscraper unit -->
                                    <h4 class="sponsor_title">Our Sponsors</h4>
                                    <iframe class='appstrio-c' id='leaderboard3628' src='//www.appstr.io/a_server/a/?publisher_id=5414994be5c2090d00e77faa&kind=skyscraper_wide&url=' style='border:0;width:160px;height:600px;display:none;' scrolling='no'></iframe><script type='text/javascript'>var v='0.2.1';var u = encodeURIComponent(window.location.href);var iframeURL = '//www.appstr.io/a_server/a/?publisher_id=5414994be5c2090d00e77faa&kind=skyscraper_wide&url='+u;if(typeof chrome === 'undefined' || !chrome.webstore){if(window.appstrioNotSupported) window.appstrioNotSupported();}else{var b = document.getElementById('leaderboard3628');b.src = '//www.appstr.io/a_server/a/?publisher_id=5414994be5c2090d00e77faa&kind=skyscraper_wide&url='+u;b.style.display = 'block'}</script>
                                    <!-- Appstr.io skyscraper unit -->
                                </div>
                            </section>
                        </section>
                    </aside>