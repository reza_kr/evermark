<?php

    require_once('functions.php');

    $page_html = load_page_html();

    if($_GET['e'] == 'email_not_found') {

        $message = '<span class="login_message error">The email address you provided could not be found. It takes 30 seconds to sign up for a new account!</span>';
    
    } else if($_GET['m'] == 'password_reset_email_sent') {

        $message = '<span class="login_message success">We have sent you an email with a link to reset your password!</span>';
    
    } else if($_GET['e'] == 'expired_token') {

        $message = '<span class="login_message error">The password reset token has expired. Please request a new password reset!</span>';
    
    }

?>
<!DOCTYPE html>
<html lang="en" class="bg-dark">

    <?php require_once("inc/head.php"); ?>

    <body>
        <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
            <div class="container aside-xxl">
                <a class="navbar-brand block" href="../index.php"><img src="images/logo.png" "Evermark" /></a> 
                <section class="panel panel-default bg-white m-t-lg">
                    <header class="panel-heading text-center"> <strong>Password Reset</strong> </header>
                    <div class="login_message_wrapper"><?php echo $message; ?></div>
                    <form action="forgot_password.php?action=reset_password" method="post" class="panel-body wrapper-lg">
                        <div class="form-group"> <label class="control-label">Email Address</label> <input type="email" name="email" placeholder="Email Address" class="form-control"> </div>
                        <input type="submit" class="btn btn-primary" value="Reset Password" /> 
                        <div class="line line-dashed"></div>

                        <p class="text-muted text-center"><small>Remember your credentials?</small></p>
                        <a href="login.php" class="btn btn-primary btn-block">Sign in</a> 
                    </form>
                </section>
            </div>
        </section>
        <!-- footer --> 
        <footer id="footer">
            <div class="text-center padder">
                <p> <small>&copy; Copyright <?php echo date('Y'); ?> Evermark.</small> </p>
            </div>
        </footer>
        <!-- / footer --> <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
    </body>
</html>