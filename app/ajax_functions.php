<?php
	
	session_start();
	// ===============================================
	//These are actions which do no require the user to be logged in....
	if($_POST['action'] == "check_username") {

		require_once("classes/Main.class.php");

		$main = new Main();

		$result = $main->checkUsername();

		echo $result;
	}

	// ==============================================


	// ==============================================
	// These are actions that require the user to be logged in...

	if($_SESSION['status'] == "active") {

		if($_POST['action'] == "add_bookmark") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->addBookmark();

			echo $result;

		} else if($_POST['action'] == "update_bookmark") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->updateBookmark();

			echo $result;

		} else if($_POST['action'] == "delete_bookmark") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->deleteBookmark();

			echo $result;

		} else if($_POST['action'] == "delete_category") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->deleteCategory();

			echo $result;
		
		} else if($_POST['action'] == "add_category") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->addCategory();

			echo $result;
		
		} else if($_POST['action'] == "get_cat_options") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->getCatOptions();

			echo $result;

		} else if($_POST['action'] == "load_new_bookmark_info") {

			require_once("classes/Main.class.php");

			$main = new Main();

			$result = $main->getStaticCopy();

			echo $result;

		}

	}
?>