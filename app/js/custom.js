$(document).ready(function() {

	$(window).resize(function() {

		checkMobile();
	});

	checkMobile();


	function checkMobile() {

		if ($(window).width() > 1000) {

		   $('body').css('overflow', 'auto');
		   $('.show_on_mobile').hide();

		} else {

		   $('body').css('overflow', 'hidden');
		   $('.show_on_mobile').show();
		}

	}


	var t_requests = new Array();

	$(".signup_form #username").keyup(function() {

		var username = $(this).val();

		if(username.length >= 3) {

			for(var i = 0; i < t_requests.length; i++) {
				t_requests[i].abort();
			}

			t_requests = [];

	    	t_requests.push(

				$.ajax({

			        url: 'ajax_functions.php',  //server script to process data
			        type: 'POST',
			        data: {
			        	action: "check_username",
			        	username: username
			        }

			    }).done(function(data) {

					if(data == "available") {	
					
						$(".signup_form #username").removeClass('taken').addClass('available');
						$(".signup_form .username_error").text(data);

					} else if(data == "taken") {

						$(".signup_form #username").removeClass('available').addClass('taken');
						$(".signup_form .username_error").text('The username is in use.');
					}

				})

			);
		
		} else {

			$(".signup_form #username").removeClass('available');
			$(".signup_form .username_error").text('');
		}

	});	


	$('.signup_form').submit(function() {

		var errors = 0;

		var username = $(".signup_form #username").val();
		var email = $(".signup_form #email").val();
		var password = $(".signup_form #password").val();

		if(username.length < 3 ) {

			errors++;
			$(".signup_form #username").addClass('error');
		
		} else {

			$(".signup_form #username").removeClass('error');
		}

		if(email == "" || !validate_email(email)) {

			errors++;
			$(".signup_form #email").addClass('error');
		
		} else {

			$(".signup_form #email").removeClass('error');
		} 

		if(password.length < 8) {

			errors++;
			$(".signup_form #password").addClass('error');
		
		} else {

			$(".signup_form #password").removeClass('error');
		} 


		if(errors > 0) {

			return false;
		
		} else {

			$(this).submit();
		}


	});


	function validate_email(email) { 
	    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(email);
	} 


	$(".search_box_toggle").click(function() {

		
		if($("#search_box").width() > 0) {

			$("#search_box").animate({
				width: 0,
				duration: 250
			});

		} else {

			$("#search_box").animate({
				width: 500,
				duration: 250
			}, function() {

				$("#search_box").focus();
			});
		}

		return false;

	});


	$("#search_box").blur(function() {

		if($("#search_box").val() == "") {

			$("#search_box").animate({
				width: 0,
				duration: 250
			});
		}
	});


	$('form').submit(function() {
		return false;
	})


	//Isotope init

	setTimeout(function() {
		// quick search regex
	  	var qsRegex;

		$(".isotope").isotope({
		  itemSelector: '.panel',
		  layoutMode: 'fitRows',
		  onLayout: function() {
            $(window).trigger("scroll");
          },
		  filter: function() {
		    return qsRegex ? $(this).find(".panel-heading, .panel-body").text().match( qsRegex ) : true;
		  }
		}).css('overflow','auto');

		// use value of search field to filter
		var $quicksearch = $('#search_box').keyup(debounce(function() {

			$('.categories li').removeClass('active');
			$('.categories li:first-child').addClass('active');

		    qsRegex = new RegExp( $quicksearch.val(), 'gi' );
		    $(".isotope").isotope({
		    	filter: function() {
				    return qsRegex ? $(this).find(".panel-heading, .panel-body").text().match( qsRegex ) : true;
				}
		    });

		    setTimeout(function() {

			    if($(".isotope .panel:visible").length == 0) {

			    	$(".no_results").fadeIn(240);
			   
			    } else {

			   		$(".no_results").fadeOut(240);
			    }

			}, 500);

		}, 200 ));


	}, 250);

	// debounce so filtering doesn't happen every millisecond
	function debounce( fn, threshold ) {
	  var timeout;
	  return function debounced() {
	    if ( timeout ) {
	      clearTimeout( timeout );
	    }
	    function delayed() {
	      fn();
	      timeout = null;
	    }
	    timeout = setTimeout( delayed, threshold || 100 );
	  }
	}


	// -----------------


	//Lazy Load

	$(".isotope .panel img").lazyload({
	    threshold : 100,
	    effect : "fadeIn"
	});

	$('body').scroll(function() {

		$(window).resize();

	});

	// -----------------


	//Infinite Scroll

	// $('.isotope').infinitescroll({
 //        navSelector  : '#pagination',    // selector for the paged navigation 
 //        nextSelector : '#pagination a',  // selector for the NEXT link (to page 2)
 //        itemSelector : '.isotope .panel',     // selector for all items you'll retrieve
 //        loading: {
 //            finishedMsg: 'No more bookmarks to load!',
 //            img: 'images/loader.gif'
 //          }
 //        },
 //        // call Isotope as a callback
 //        function(newItems) {
 //          $('.isotope').isotope('appended', $(newItems)); 
 //        }
 //     );

	// -----------------

	// Load categories
	$.ajax({

        url: 'ajax_functions.php',  //server script to process data
        type: 'POST',
        data: {
        	action: "get_cat_options"
        }

    }).done(function(data) {

		$('.add_bookmark_wrapper #category').html(data);

	});


	$('body').on('click', '.static_copy', function() {

		var bookmark_title = $(this).closest(".panel").find('.bookmark_title').text();
		var static_url = $(this).attr("href");
		var date = $(this).attr("data-date");

		if(static_url != "") {
			load_static_viewer(bookmark_title, static_url, date);
		}

		return false;

	});


	function load_static_viewer(bookmark_title, static_url, date) {

		$('.add_bookmark_wrapper').hide();

		var height = $(window).height();

		$('.isotope').fadeOut(500);

		$('.static_viewer').show();

		$('.static_viewer').animate({
			height: height - 75
		});

		$('iframe#static_viewer_frame').animate({
			height: height - 115
		}, function() {

			$('.static_viewer .static_viewer_header .page_title').text(bookmark_title + ' | Static Copy (' + date + ')');

			$('iframe#static_viewer_frame').attr("src", static_url);
		});

	}


	$(window).resize(function() {

		var height = $(window).height();

		$('.static_viewer').animate({
			height: height - 75
		});

		$('iframe#static_viewer_frame').animate({
			height: height - 115
		});

	});


	$('.close_static_viewer').click(function() {

		$('.isotope').fadeIn('fast', function() {
			$('.isotope').isotope();
			$('.add_bookmark_wrapper').fadeIn('fast');
		});

		$('.static_viewer').animate({
			height: 0
		}, function() {

			$('.static_viewer').fadeOut('fast');
		});

		$('iframe#static_viewer_frame').animate({
			height: 0
		});

		$('iframe#static_viewer_frame').attr("src", "");

		return false;
	});



	// $('.add_bookmark').click(function() {

	// 	clear_popups();

	// 	$('.isotope').css('margin-top', '55px');

	// 	$('.add_bookmark_wrapper').show();

	// 	$('.add_bookmark_wrapper #category').html('<option>(Loading)</option>');

	// 	var current_cat = $('.categories li.active').attr('id');

	// 	$.ajax({

	//         url: 'ajax_functions.php',  //server script to process data
	//         type: 'POST',
	//         data: {
	//         	action: "get_cat_options"
	//         }

	//     }).done(function(data) {

	// 		$('.add_bookmark_wrapper #category').html(data);

	// 	});

	// 	return false;
	// });


	$('#save_bookmark').click(function() {

		add_bookmark();

		return false;
	});

	$('.add_bookmark_wrapper form').submit(function() {

		add_bookmark();

		return false;
	});


	function add_bookmark() {

		var bookmark_title = $('.add_bookmark_wrapper #bookmark_title').val();
		var bookmark_url = $('.add_bookmark_wrapper #bookmark_url').val();
		var bookmark_cat = $('.add_bookmark_wrapper #category option:selected').val();

		bookmark_title = bookmark_title.trim();
		bookmark_url = bookmark_url.trim();

		if(bookmark_url != '') {

			$('.add_bookmark_wrapper #bookmark_title').prop('disabled', true);
			$('.add_bookmark_wrapper #bookmark_url').prop('disabled', true);
			$('.add_bookmark_wrapper #category').prop('disabled', true);
			$('.add_bookmark_wrapper #save_bookmark').prop('disabled', true);
			$('.add_bookmark_wrapper .spinner').show();

			$('.isotope .welcome').fadeOut('fast');

			$.ajax({

		        url: 'ajax_functions.php',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "add_bookmark",
		        	bookmark_title: bookmark_title,
		        	bookmark_url: bookmark_url,
		        	bookmark_cat: bookmark_cat
		        }

		    }).done(function(data) {

		    	if(data == 'not_allowed') {

		    		$('.add_bookmark_wrapper .spinner').hide();

					$('.add_bookmark_wrapper #bookmark_title').prop('disabled', false);
					$('.add_bookmark_wrapper #bookmark_url').prop('disabled', false);
					$('.add_bookmark_wrapper #category').prop('disabled', false);
					$('.add_bookmark_wrapper #save_bookmark').prop('disabled', false);

					$('#not_allowed').fadeIn('fast');
					setTimeout(function() {
						$('#not_allowed').fadeOut('fast');
					}, 2500);

		    	} else if(data == 'url_unreachable') {

		    		$('.add_bookmark_wrapper .spinner').hide();

					$('.add_bookmark_wrapper #bookmark_title').prop('disabled', false);
					$('.add_bookmark_wrapper #bookmark_url').prop('disabled', false);
					$('.add_bookmark_wrapper #category').prop('disabled', false);
					$('.add_bookmark_wrapper #save_bookmark').prop('disabled', false);

					$('#url_unreachable').fadeIn('fast');
					setTimeout(function() {
						$('#url_unreachable').fadeOut('fast');
					}, 2500);

		    	} else {	

					$('.add_bookmark_wrapper #bookmark_title').val('');
					$('.add_bookmark_wrapper #bookmark_url').val('');
					$('.add_bookmark_wrapper #category').val('');

					$('.add_bookmark_wrapper .spinner').hide();

					$('.add_bookmark_wrapper #bookmark_title').prop('disabled', false);
					$('.add_bookmark_wrapper #bookmark_url').prop('disabled', false);
					$('.add_bookmark_wrapper #category').prop('disabled', false);
					$('.add_bookmark_wrapper #save_bookmark').prop('disabled', false);

					$(".isotope").prepend(data).isotope('reloadItems').isotope();

					bookmark_id = $('.isotope .panel:first-child').attr('id');

					$.ajax({

				        url: 'ajax_functions.php',  //server script to process data
				        type: 'POST',
				        timeout: 8000,
				        data: {
				        	action: "load_new_bookmark_info",
				        	bookmark_id: bookmark_id
				        },
				        error: function() { 

					    	$('.isotope .panel#' + bookmark_id + ' .panel-body img').attr('src', 'images/placeholder.png');

					    	$('.isotope .panel#' + bookmark_id + ' .loader').remove();

				        },
				        success: function(data) {

				        	data_args = data.split(',');

					    	bookmark_img = data_args[0];
					    	bookmark_static_url = data_args[1];

					    	$('.isotope .panel#' + bookmark_id + ' .panel-body img').attr('src', bookmark_img);
					    	$('.isotope .panel#' + bookmark_id + ' .panel-footer .static_copy').attr('href', bookmark_static_url);

					    	if(bookmark_static_url != '') {
					    		$('.isotope .panel#' + bookmark_id + ' .panel-footer .static_copy').removeClass('disabled');
					    	}

					    	$('.isotope .panel#' + bookmark_id + ' .loader').remove();
				        }

				    });

				}

			});
		}
	} 


	// $('body').on('click', '.edit_bookmark', function() {

	// 	clear_popups();

	// 	$('.isotope').css('margin-top', '55px');

	// 	$('.isotope .panel').removeClass('edit_mode');
	// 	$(this).closest('section').addClass('edit_mode');

	// 	var bookmark_id = $(this).closest('section').attr('id');
	// 	var bookmark_title = $(this).closest('section').find('.bookmark_title').text();
	// 	var bookmark_url = $(this).closest('section').find('.bookmark_url').text();
	// 	var bookmark_cat = $(this).closest('section').attr('data-cat');

	// 	$('.edit_bookmark_wrapper #bookmark_title').val(bookmark_title);
	// 	$('.edit_bookmark_wrapper #bookmark_url').val(bookmark_url);
	// 	$('.edit_bookmark_wrapper #bookmark_id').val(bookmark_id);

	// 	$('.edit_bookmark_wrapper').show();

	// 	$('.edit_bookmark_wrapper #category').html('<option>(Loading)</option>');

	// 	$.ajax({

	//         url: 'ajax_functions.php',  //server script to process data
	//         type: 'POST',
	//         data: {
	//         	action: "get_cat_options"
	//         }

	//     }).done(function(data) {

	// 		$('.edit_bookmark_wrapper #category').html(data);

	// 		if(bookmark_cat != 0) {
	// 			$('.edit_bookmark_wrapper #category').val(bookmark_cat);
	// 		}

	// 	});

	// 	return false;

	// });


	// $('#update_bookmark').click(function() {

	// 	$('.isotope .panel').removeClass('edit_mode');

	// 	update_bookmark();

	// 	return false;

	// });


	// $('.edit_bookmark_wrapper form').submit(function() {

	// 	update_bookmark();

	// 	return false;
	// });	


	// function update_bookmark() {

	// 	var bookmark_id = $('.edit_bookmark_wrapper #bookmark_id').val();
	// 	var bookmark_title = $('.edit_bookmark_wrapper #bookmark_title').val();
	// 	var bookmark_url = $('.edit_bookmark_wrapper #bookmark_url').val();
	// 	var bookmark_cat = $('.edit_bookmark_wrapper #category option:selected').val();

	// 	$('.edit_bookmark_wrapper #bookmark_title').prop('disabled', true);
	// 	$('.edit_bookmark_wrapper #bookmark_url').prop('disabled', true);
	// 	$('.edit_bookmark_wrapper #category').prop('disabled', true);
	// 	$('.edit_bookmark_wrapper #update_bookmark').prop('disabled', true);
	// 	$('.edit_bookmark_wrapper .spinner').show();

	// 	$.ajax({

	//         url: 'ajax_functions.php',  //server script to process data
	//         type: 'POST',
	//         data: {
	//         	action: "update_bookmark",
	//         	bookmark_id: bookmark_id,
	//         	bookmark_title: bookmark_title,
	//         	bookmark_url: bookmark_url,
	//         	bookmark_cat: bookmark_cat
	//         }

	//     }).done(function(data) {

	// 		//... hmmmm what to do?
	// 		$('.edit_bookmark_wrapper #bookmark_title').val('');
	// 		$('.edit_bookmark_wrapper #bookmark_url').val('');
	// 		$('.edit_bookmark_wrapper #bookmark_id').val('');
	// 		$('.edit_bookmark_wrapper #category').val('');

	// 		$('.edit_bookmark_wrapper .spinner').hide();

	// 		$('.edit_bookmark_wrapper').hide();

	// 		$('.edit_bookmark_wrapper #bookmark_title').prop('disabled', false);
	// 		$('.edit_bookmark_wrapper #bookmark_url').prop('disabled', false);
	// 		$('.edit_bookmark_wrapper #category').prop('disabled', false);
	// 		$('.edit_bookmark_wrapper #update_bookmark').prop('disabled', false);

	// 		$('.isotope').css('margin-top', '0');

	// 	});

	// }


	// $('.close_edit_bookmark').click(function() {

	// 	$('.edit_bookmark_wrapper').hide();

	// 	$('.isotope .panel').removeClass('edit_mode');

	// 	$('.isotope').css('margin-top', '0');

	// 	return false;
	// });


	$('body').on('click', '.delete_bookmark', function() {

		var bookmark_id = $(this).closest('section').attr('id');

		$.ajax({

	        url: 'ajax_functions.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "delete_bookmark",
	        	bookmark_id: bookmark_id
	        }

	    }).done(function(data) {

			$('.isotope section#' + bookmark_id).fadeOut(240, function() {

				$(".isotope").isotope('remove', $(this));

				$(".isotope").isotope('layout');
			});

		});

		return false;

	});


	// Add category.......
	$('#open_add_category').click(function() {

		if($('.add_category_wrapper').is(':visible')) {

			$('.add_category_wrapper').slideUp(240);
		
		} else {

			$('.add_category_wrapper').slideDown(240);
		}
	});


	$('#add_category').click(function() {

		add_category();

		return false;
	});

	$('.add_category_wrapper form').submit(function() {

		add_category();

		return false;
	});


	function add_category() {

		var cat_name = $('.add_category_wrapper #cat_name').val();

		$.ajax({

	        url: 'ajax_functions.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "add_category",
	        	cat_name: cat_name
	        }

	    }).done(function(data) {

			if(data != "failed") {

				$(".categories li:last").before(data);
			}

			$('.add_category_wrapper #cat_name').val("");
			$('.add_category_wrapper').slideUp(240);

			//RELOAD CATEGORIES DROPDOWN
			$.ajax({

		        url: 'ajax_functions.php',  //server script to process data
		        type: 'POST',
		        data: {
		        	action: "get_cat_options"
		        }

		    }).done(function(data) {

				$('.add_bookmark_wrapper #category').html(data);

			});

		});

	}


	$('body').on('click', '.delete_category', function() {

		if ($(this).closest('li').find('.confirm_cat_deletion').length <= 0) {

			$(this).closest('li').append('<div class="confirm_cat_deletion">Delete Category? <a href="#" id="yes" class="btn btn-primary btn-small">Yes</a> <a href="#" id="no" class="btn btn-primary btn-small">No</a></div>');

			$(this).closest('li').find('.confirm_cat_deletion').slideDown(240);
		
		} else {

			$(this).closest('li').find('.confirm_cat_deletion').slideUp(240, function() {

				$(this).remove();
			});
		}

		return false;
	});

	$('body').on('click', '.confirm_cat_deletion #no', function() {

		$(this).closest('li').find('.confirm_cat_deletion').slideUp(240, function() {

			$(this).remove();
		});

		return false;

	});

	$('body').on('click', '.confirm_cat_deletion #yes', function() {

		var li = $(this).closest('li');
		var li_cat_id = $(li).attr('id');

		var cat_id_parts = li_cat_id.split('-');
		cat_id = cat_id_parts[1];

		$.ajax({

	        url: 'ajax_functions.php',  //server script to process data
	        type: 'POST',
	        data: {
	        	action: "delete_category",
	        	cat_id: cat_id
	        }

	    }).done(function(data) {

			if(data == "success") {

				if($(li).is('.active')) {

					$('.categories li:first-child > a').trigger('click');
				}
			
				$('.categories li#' + li_cat_id).slideUp(240, function() {

					$(this).remove();
				});
			}

		});

		return false;

	});


	$('body').on('click', '.categories li > a', function() {

		$('.categories li').removeClass('active');
		$(this).closest('li').addClass('active');

		var id = $(this).closest('li').attr('id');

		if(id != '' && typeof id != 'undefined') {

			id = '.' + id;

			$('.isotope').isotope({ filter: id });

		} else {

			$('.isotope').isotope({ filter: '*' });
		}

		$('#search_box').val('');
		$("#search_box").animate({
			width: 0,
			duration: 250
		});

		return false;
	});


	$('body').on('click', '.dropdown-menu li a', function() {

		$(this).closest('.open').removeClass('open');
	});

});