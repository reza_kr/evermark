<?php	

	//Evermark -- Chrome.class.php

	session_start();

	Class Chrome { 

			public function __construct () {

				require("inc/connect_dbo.php");

				$this->dbo = $dbo;

			}


			public function login() {


				session_unset();  // destroy $_SESSION ram

				session_destroy();  // destroy $_SESSION file

				session_write_close();


		    	$username = $_POST['username'];

		    	$password = $_POST['password'];
				
				$hash_password = hash('SHA512', $password);


				$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE username = ? AND password = ? LIMIT 1");
				$query->execute(array($username, $hash_password));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$num_rows = $row[0];
				}


				if($num_rows > 0) {


					$query = $this->dbo->prepare("SELECT user_id, username, email, verified FROM users WHERE username = ? AND password =  ?LIMIT 1");
					$query->execute(array($username, $hash_password));

					$result = $query->fetchAll();

					foreach($result as $row) {

						$username = $row[1];
						$verified = $row[3];

						if($verified == 1) {

				    		// $_SESSION['user_id'] = $row[0];
				    		// $_SESSION['username'] = $row[1];
				    		// $_SESSION['email'] = $row[2];

				    		// $_SESSION['status'] = "active";

				    		// $_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR'];

							setcookie("evermark_chrome_ext", "active", time() + 2592000, '/');
							setcookie("evermark_chrome_ext_id", $row[0], time() + 2592000, '/');

				    		return $username;

				    	} else {

				    		return "verification_required";
				    	}

					}


				} else {

					return "login_failed";

				}

		    }


		    function signOut() {

		    	setcookie("evermark_chrome_ext", "active", time() - 3600, '/');
				setcookie("evermark_chrome_ext_id", $row[0], time() - 3600, '/');

		    }


		    public function checkSession() {

		    	if(isset($_COOKIE['evermark_chrome_ext']) && $_COOKIE['evermark_chrome_ext'] == 'active' && isset($_COOKIE['evermark_chrome_ext_id'])) {
		    		
		    		$user_id = $_COOKIE['evermark_chrome_ext_id'];

		    		$query = $this->dbo->prepare("SELECT username FROM users WHERE user_id = ?");
					$query->execute(array($user_id));

					$result = $query->fetchAll();

					foreach($result as $row) {

						$username = $row[0];
					}

					return $username;

		    	} else {

		    		return '';
		    	}
		    }


			public function saveBookmark() {

				ignore_user_abort(true);
				set_time_limit(60);

				$user_id = $_COOKIE['evermark_chrome_ext_id'];
				$username = 'reza_kr';

				$direct_download_exts = ['png', 'jpg', 'gif', 'pdf', 'js', 'jpeg', 'css', 'swf'];
				$disallowed_exts = ['mp3', 'wav', 'alac', 'flac', 'aiff', 'avi', 'mp4', 'mov', 'flv', 'wmv', 'm4v', 'mpeg', 'zip', 'rar', 'torrent', 'tar', 'tar.gz', 'gz'];
				//$allowed_exts = ['php', 'html', 'htm', 'aspx', 'asp'];

				$bookmark_title = htmlspecialchars($_POST['title']);
				$bookmark_url = $_POST['url'];
				$bookmark_cat = $_POST['cat'];
				$browser = $_POST['browser'];

				$bookmark_url_ext = pathinfo($bookmark_url, PATHINFO_EXTENSION);

				if(!$bookmark_url_ext || !in_array($bookmark_url_ext, $disallowed_exts)) { //If there is no extension or the extension is in the allowed list.. continue

					if (!preg_match("~^(?:f|ht)tps?://~i", $bookmark_url)) {
				        $bookmark_url = "http://" . $bookmark_url;
				    }

				    if(empty($bookmark_title)) {

						$url = $bookmark_url;

						$ch = curl_init();

					    curl_setopt($ch, CURLOPT_HEADER, 0);
					    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					    curl_setopt($ch, CURLOPT_URL, $url);
					    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

					    $html = curl_exec($ch);

					    $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

					    curl_close($ch);

					    $url = $last_url;


						//parsing begins here:
						$doc = new DOMDocument();
						@$doc->loadHTML($html);
						$nodes = $doc->getElementsByTagName('title');

						//get and display what you need:
						$title = $nodes->item(0)->nodeValue;

						$bookmark_title = $title;
					}

					$today = date("Y-m-d H:i:s");
					$date_added = date('d/m/Y');

					$query = $this->dbo->prepare("INSERT INTO bookmarks SET user_id = ?, title = ?, url = ?, cat = ?, date_added = ?, type = ?, browser = ?");
					$query->execute(array($user_id, $bookmark_title, $bookmark_url, $bookmark_cat, $today, 'page', $browser));

					$bookmark_id = $this->dbo->lastInsertId();
		        
		        } else if(in_array($bookmark_url_ext, $direct_download_exts)) { //Otherwise, if extension is a allowed direct download.... then download it straight.

		        	if (!preg_match("~^(?:f|ht)tps?://~i", $bookmark_url)) {
				        $bookmark_url = "http://" . $bookmark_url;
				    }

				    if(empty($bookmark_title)) {

						$bookmark_title = basename($bookmark_url);
					}

					$today = date("Y-m-d H:i:s");
					$date_added = date('d/m/Y');

					$query = $this->dbo->prepare("INSERT INTO bookmarks SET user_id = ?, title = ?, url = ?, cat = ?, date_added = ?, type = ?, ext = ?, browser = ?");
					$query->execute(array($user_id, $bookmark_title, $bookmark_url, $bookmark_cat, $today, 'file', $bookmark_url_ext, $browser));

					$bookmark_id = $this->dbo->lastInsertId();
		        	
		        } else {

		        	return 'not_allowed';
		        }


		        // ============================



		        require_once("S3.class.php");
				$s3 = new S3();

				//$bookmark_id = $_POST['bookmark_id'];

				$query = $this->dbo->prepare("SELECT url, type, ext FROM bookmarks WHERE user_id = ? AND id = ?");
				$query->execute(array($user_id, $bookmark_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$url = $row[0];
		    		$type = $row[1];
		    		$ext = $row[2];
		    	}

		    	$url_parsed = parse_url($url);
				$url_base = $url_parsed[host];

		    	if($type == 'page') {

					$file_path = '/var/www/html/tmp_files/' . $username . '_' . $url_base . '/static.html';

					$new_filename = $bookmark_id . '_' . $url_base . '.html';

					exec('httrack "' . $url . '" -O "/var/www/html/tmp_files/' . $username . '_' . $url_base . '" "+*.' . $url_base . '/*" -v -r1 -N "static.html"', $output);


					$static_url = $s3->putObject($username, $new_filename, $file_path, $bookmark_id);
					$static_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $static_url);

					chdir('../tmp_files');
					exec('phantomjs ../app/scripts/screenshot.js ' . $url . ' ' . $bookmark_id);

					$new_img = $bookmark_id . '_' . $url_base . '.png';
					$img_path = '/var/www/html/tmp_files/' . $bookmark_id . '_screenshot.png';

					exec('convert ' . $img_path . ' -resize 30% ' . $img_path);


					$img_url = $s3->putObject($username, $new_img, $img_path, $bookmark_id);
					$img_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $img_url);


					$query = $this->dbo->prepare("UPDATE bookmarks SET static_url = ?, thumbnail = ? WHERE user_id = ? AND id = ?");
					$query->execute(array($static_url, $img_url, $user_id, $bookmark_id));

				} else if($type == 'file') {

					file_put_contents("../tmp_files/" . $username . '_' . basename($url), fopen($url, 'r'));

					$file_path = '/var/www/html/tmp_files/' . $username . '_' . basename($url);

					$new_filename = $bookmark_id . '_' . basename($url);


					$img_ext_array = ['png', 'jpg', 'jpeg', 'gif'];
					if(in_array($ext, $img_ext_array)) {

						$new_img = $bookmark_id . '_' . basename($url);
						$new_img_thumb = 'thumb_' . $bookmark_id . '_' . basename($url);
						$img_path = '/var/www/html/tmp_files/' . $username . '_' . basename($url);
						$img_path_resized = '/var/www/html/tmp_files/thumb_' . $username . '_' . basename($url);

						exec('convert ' . $img_path . ' -resize 320x500 -gravity center -extent 320x215 ' . $img_path_resized);


						//Thumbnail
						$resized_img_url = $s3->putObject($username, $new_img_thumb, $img_path_resized, $bookmark_id);
						$resized_img_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $resized_img_url);

						//Fullsize
						$fullsize_img_url = $s3->putObject($username, $new_img, $img_path, $bookmark_id);
						$fullsize_img_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $fullsize_img_url);

						$query = $this->dbo->prepare("UPDATE bookmarks SET static_url = ?, thumbnail = ? WHERE user_id = ? AND id = ?");
						$query->execute(array($fullsize_img_url, $resized_img_url, $user_id, $bookmark_id));
					
					} else {

						$img_url = "images/file_placeholder.png";

						$static_url = $s3->putObject($username, $new_filename, $file_path, $bookmark_id);
						$static_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $static_url);

						$query = $this->dbo->prepare("UPDATE bookmarks SET static_url = ?, thumbnail = ? WHERE user_id = ? AND id = ?");
						$query->execute(array($static_url, $img_url, $user_id, $bookmark_id));

					}

				}

				return $bookmark_title . ',' . $bookmark_url;

			}


			public function loadBookmarks() {

				$user_id = $_COOKIE['evermark_chrome_ext_id'];

				$query = $this->dbo->prepare("SELECT id, title, url FROM bookmarks WHERE user_id = ? ORDER BY date_added DESC");
				$query->execute(array($user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$disable_static = '';

		    		$id = $row[0];
		    		$title = $row[1];
		    		$url = $row[2];

		    		$date_added = date("d/m/Y", strtotime($date_added));

		    		if($static_url == "") {
		    			$disable_static = 'disabled';
		    		}


		    		$bookmarks .= '<li class="bookmark"><a href="' . $url . '" target="_blank" title="' . $title . '">' . $title . ' <span class="faded">| ' . $url . '</span></a></li>';

				}


				return $bookmarks;

			}



			public function loadCategories() {

				$user_id = $_COOKIE['evermark_chrome_ext_id'];

				$categories = '<option value="">Category</option>';

				$query = $this->dbo->prepare("SELECT id, cat_name FROM categories WHERE user_id = ? ORDER BY cat_name ASC");
				$query->execute(array($user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$id = $row[0];
		    		$cat_name = $row[1];

		    		$categories .= '<option value="' . $id . '">' . $cat_name . '</option>';

				}

				return $categories;

			}

	}
?>