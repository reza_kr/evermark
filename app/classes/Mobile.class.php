<?php	
	//Evermark -- Mobile.class.php
	ini_set('display_errors', 1); 
	error_reporting(E_ALL);
	session_start();

	Class Mobile { 

			public function __construct () {

				require("inc/connect_dbo.php");

				$this->dbo = $dbo;

			}


			public function addToMailingList() {

		    	$email = $_POST['email'];

		    	$query = $this->dbo->prepare("SELECT COUNT(*) FROM mailing_list WHERE email = ?");
				$query->execute(array($email));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$num_rows = $row[0];
				}

				
				if($num_rows <= 0) {

					$query = $this->dbo->prepare("INSERT INTO mailing_list SET email = ?");
					$query->execute(array($email));

			   	}

		    }



		    public function checkUsername() {

		    	$username = $_POST['username'];

		    	$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE username = ?");
				$query->execute(array($username));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$num_rows = $row[0];
				}
				
				if($num_rows > 0) {

					return 'taken';
				
				} else {

					return 'available';
				}

		    }



		    public function register() {

		    	require_once("classes/Email.class.php");
				$phpMailer = new Email();

				$username = $_POST['username'];
				$email = $_POST['email'];
				$password = $_POST['password'];

				if(strlen($username) >= 3) {

					if(strlen($password) >= 8) {

						$password = addslashes($password);

						$today = date("Y-m-d H:i:s");

						$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE email = ?");
						$query->execute(array($email));

						$result = $query->fetchAll();

						foreach($result as $row) {

							$num_rows = $row[0];
						}
						
						if($num_rows <= 0) {


							$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE username = ?");
							$query->execute(array($username));

							$result = $query->fetchAll();

							foreach($result as $row) {

								$num_rows = $row[0];
							}
							
							if($num_rows <= 0) {

								$hash_password = hash('SHA512', $password);
								$token = hash('SHA512', $email);
								
								$query = $this->dbo->prepare("INSERT INTO users SET username = ?, email = ?, password = ?, date_created = ?, token = ?");
								$query->execute(array($username, $email, $hash_password, $today, $token));

								/* Send Email */
								$phpMailer->register($username, $email, $token);
								
								header('Location: login.php?m=confirm_email');

							} else {

								header('Location: signup.php?e=username_exists');
							}
						
						} else {

							header('Location: signup.php?e=email_exists');
						}

					} else {

						header('Location: signup.php?e=insecure_password');
					}

				} else {

					header('Location: signup.php?e=username_too_short');
				}
			}


			public function signupConfirmation() {

				$token = $_GET['token'];
				$email = $_GET['email'];

				$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE email = ? AND token = ?");
				$query->execute(array($email, $token));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$num_rows = $row[0];
				}
				
				if($num_rows > 0) {

					$query = $this->dbo->prepare("UPDATE users SET verified = ? WHERE email = ? AND token = ?");
					$query->execute(array(1, $email, $token));

					header('Location: signup_confirmation.php?m=verified');

				} else {

					header('Location: signup_confirmation.php?e=verification_failed');
				}

			}


			public function login() {

		    	$username = $_POST['username'];

		    	$password = $_POST['password'];
				
				$hash_password = hash('SHA512', $password);


				$query = $this->dbo->prepare("SELECT COUNT(*) FROM users WHERE username = ? AND password = ? LIMIT 1");
				$query->execute(array($username, $hash_password));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$num_rows = $row[0];
				}


				if($num_rows > 0) {


					$query = $this->dbo->prepare("SELECT user_id, username, email, verified FROM users WHERE username = ? AND password =  ?LIMIT 1");
					$query->execute(array($username, $hash_password));

					$result = $query->fetchAll();

					foreach($result as $row) {

						$verified = $row[3];

						if($verified == 1) {

				    		$_SESSION['user_id'] = $row[0];
				    		$_SESSION['username'] = $row[1];
				    		$_SESSION['email'] = $row[2];

				    		$_SESSION['status'] = "active";

				    		$_SESSION['ip_address'] = $_SERVER['REMOTE_ADDR'];

				    		header('Location: dashboard.php');

				    	} else {

				    		header('Location: login.php?e=verification_required');
				    	}

					}


				} else {

					header('Location: login.php?e=login_failed');

				}

		    }



		    public function logout($msg) {

		    	$user_id = $_SESSION['user_id'];

				session_unset();  // destroy $_SESSION ram

				session_destroy();  // destroy $_SESSION file

				session_write_close();

				header('Location: login.php');

		    }



		    public function resetPassword() {

		    	require_once("classes/Email.class.php");
				$phpMailer = new Email();

		    	$email = $_POST['email'];

		    	$found = false;


				$query = $this->dbo->prepare("SELECT user_id, username FROM users WHERE email = ?");
				$query->execute(array($email));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$found = true;
				    
				    $user_id = $row[0];
				    $username = $row[1];

				    $token = $this->randomToken();

				    $token_hash = hash('SHA512', $token);

				    $datetime = date('Y-m-d H:i:s', time() + 86400); //NOW PLUS 1 DAY for EXPIRY

				    $query = $this->dbo->prepare("INSERT INTO password_reset_token SET user_id = ?, email = ?, token = ?, expiry = ?");
					$query->execute(array($user_id, $email, $token_hash, $datetime));

					$phpMailer->resetPassword($username, $email, $token_hash);

					header('Location: forgot_password.php?m=password_reset_email_sent');

				}


				if($found == false) {

					header('Location: forgot_password.php?e=email_not_found');
					
				}


				

		    }


		    public function randomToken($length = 20) {
			    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
			    $randomString = '';
			    for ($i = 0; $i < $length; $i++) {
			        $randomString .= $characters[rand(0, strlen($characters) - 1)];
			    }
			    return $randomString;
			}



			public function checkPasswordResetToken() {

				$token = $_GET['token'];
				$email = html_entity_decode($_GET['email']);

				$num_rows = 0;

				$datetime = date('Y-m-d H:i:s');

				$query = $this->dbo->prepare("SELECT email FROM password_reset_token WHERE email = ? AND token = ? AND expiry >= ?");
				$query->execute(array($email, $token, $datetime));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$found_email = $row[0];

				}

				if($found_email != "") {

					return true;

				} else {

					header('Location: forgot_password.php?e=expired_token' . $found_email);
				}

			}



			public function resetPasswordChange($email) {

				require_once("classes/Email.class.php");
				$phpMailer = new Email();

				$new_password = $_POST['new_password'];
				$new_password2 = $_POST['new_password2'];

				$token = $_GET['token'];


				if($new_password == $new_password2) {

					//if (preg_match('/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/', $new_password)) {
					if(strlen($new_password) >= 8) {

						$new_password = hash('SHA512', $new_password);

						$query = $this->dbo->prepare("UPDATE users SET password = '$new_password' WHERE email = ?");
						$query->execute(array($email));


						$query = $this->dbo->prepare("SELECT username FROM users WHERE email = ?");
						$query->execute(array($email));

						$result = $query->fetchAll();

						foreach($result as $row) {

							$username = $row['username'];
						}


				    	$query = $this->dbo->prepare("DELETE FROM password_reset_token WHERE email = ?");
						$query->execute(array($email));

						$phpMailer->confirmPasswordReset($username, $email);

				    	header('Location: password_change.php?token=' . $token . '&email=' . $email . '&r=password_change_successful');

					} else {

						header('Location: password_change.php?token=' . $token . '&email=' . $email . '&e=insecure_password');
					}

				} else {

					header('Location: password_change.php?token=' . $token . '&email=' . $email . '&e=password_mismatch');
				}
			}



			public function changePassword() {

				require_once("classes/Email.class.php");
				$phpMailer = new Email();

				$email = $_POST['email'];
				$token = $_POST['token'];
				$new_password = $_POST['password1'];
				$new_password2 = $_POST['password2'];


				if($new_password == $new_password2) {

					if(strlen($new_password) >= 8) {

						$new_password = hash('SHA512', $new_password);

						$query = $this->dbo->prepare("UPDATE users SET password = ? WHERE email = ?");
						$query->execute(array($new_password, $email));

						$query = $this->dbo->prepare("DELETE FROM password_reset_token WHERE email = ?");
						$query->execute(array($email));

						$phpMailer->confirmPasswordReset($username, $email);


				    	header('Location: login.php?m=password_changed');

					} else {

						header('Location: password_change.php?e=insecure_password');
					}

				} else {

					header('Location: password_change.php?e=password_mismatch');
				}

			}


			public function checkSession() {
				return 'inactive';
				// if(isset($_SESSION['status']) && $_SESSION['status'] == 'active') {

				// 	return 'active';
				
				// } else {

				// 	return 'inactive';
				// }

			}


			public function loadCategories() {

				$user_id = $_SESSION['user_id'];

				$query = $this->dbo->prepare("SELECT id, cat_name, color FROM categories WHERE user_id = ? ORDER BY cat_name ASC");
				$query->execute(array($user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$id = $row[0];
		    		$cat_name = $row[1];
		    		$color = $row[2];

		    		$categories .= '<li id="cat-' . $id . '"> <a href="#" title="' . $cat_name . '"><i class="fa fa-chevron-right icon"> <b style="background: ' . $color . '"></b> </i> <span>' . $cat_name . '</span> <i class="fa fa-trash-o delete_category"></i></a></li>';

				}

				return $categories;

			}


			public function getCatOptions() {

				$user_id = $_SESSION['user_id'];

				$categories = '<option value="">Category</option>';

				$query = $this->dbo->prepare("SELECT id, cat_name FROM categories WHERE user_id = ? ORDER BY cat_name ASC");
				$query->execute(array($user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$id = $row[0];
		    		$cat_name = $row[1];

		    		$categories .= '<option value="' . $id . '">' . $cat_name . '</option>';

				}

				return $categories;

			}


			public function addCategory() {

				$colors = array('#c679ad', '#d97862', '#eb9262', '#ebb262', '#eecf68', '#f2e469', '#ccd56e', '#7fac76', '#82afb3', '#7792d3', '#9787c1', '#996ead');
				$color = $colors[rand(0, count($colors) - 1)];

				$user_id = $_SESSION['user_id'];

				$cat_name = $_POST['cat_name'];

				$query = $this->dbo->prepare("INSERT INTO categories SET user_id = ?, cat_name = ?, color = ?");
				$query->execute(array($user_id, $cat_name, $color));

				$cat_id = $this->dbo->lastInsertId();

				return '<li id="' . $cat_id . '"> <a href="#" title="' . $cat_name . '"><i class="fa fa-chevron-right icon"> <b style="background: ' . $color . '"></b> </i> <span>' . $cat_name . '</span> <i class="fa fa-trash-o delete_category"></i></a></li>';

			}


			public function deleteCategory() {

				$user_id = $_SESSION['user_id'];

				$cat_id = $_POST['cat_id'];

				$query = $this->dbo->prepare("DELETE FROM categories WHERE user_id = ? AND id = ?");
				$query->execute(array($user_id, $cat_id));

				$query = $this->dbo->prepare("UPDATE bookmarks SET cat = ? WHERE cat = ?");
				$query->execute(array(0, $cat_id));

				return 'success';

			}


			public function loadBookmarks() {

				$user_id = $_SESSION['user_id'];
				$user_id = 23;

				$query = $this->dbo->prepare("SELECT id, title, url, static_url, thumbnail, cat, date_added FROM bookmarks WHERE user_id = ? ORDER BY date_added DESC");
				$query->execute(array($user_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

					$disable_static = '';

		    		$id = $row[0];
		    		$title = $row[1];
		    		$url = $row[2];
		    		$static_url = $row[3];
		    		$thumbnail = $row[4];
		    		$cat = $row[5];
		    		$date_added = $row[6];

		    		$date_added = date("d/m/Y", strtotime($date_added));

		    		if($static_url == "") {
		    			$disable_static = 'disabled';
		    		}


		    		$bookmarks .= '<section class="panel panel-default cat-' . $cat . '" id="' . $id . '" data-cat="' . $cat . '">
	                                    <header class="panel-heading font-bold"><a href="' . $url . '" target="_blank" class="bookmark_title" title="' . $title . '">' . $title . '</a></header>
	                                    <div class="panel-body">
	                                        <a href="' . $url . '" target="_blank"><img src="images/placeholder.png" data-original="' . $thumbnail . '"/></a>

	                                        <div class="url"><a href="' . $url . '" target="_blank" class="bookmark_url" title="' . $url . '">' . $url . '</a></div>
	                                    </div>
	                                    <footer class="panel-footer bg-white no-padder">
	                                        <div class="row text-center no-gutter">
	                                            <div class="col-xs-5 b-r b-light"><a href="' . $url . '" target="_blank" class="h4 live_site">Live Site</a></div>
	                                            <div class="col-xs-5 b-r b-light"><a href="' . $static_url . '" target="_blank" class="h4 static_copy ' . $disable_static . '" data-date="' . $date_added . '">Static Copy</a></div>
	                                            <div class="col-xs-2 b-r b-light">
	                                                <a href="#" class="h4 dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></a>
	                                                <ul class="dropdown-menu animated fadeInRight">
	                                                    <span class="arrow top"></span> 
	                                                    <li><a href="#" class="delete_bookmark"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete Bookmark</a></li>
	                                                </ul>
	                                            </div>
	                                        </div>
	                                    </footer>
	                                </section>';
	                                //<li><a href="#" class="update_static_copy"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Update Static Copy</a></li>
				}

				if($bookmarks == "") {

					$bookmarks = '<div class="welcome">
                                    <img src="images/welcome_logo.png"/>
                                </div>';

				}


				return $bookmarks;

			}


			public function addBookmark() {

				$user_id = $_SESSION['user_id'];
				$direct_download_exts = ['png', 'jpg', 'gif', 'pdf', 'js', 'jpeg', 'css', 'swf'];
				$disallowed_exts = ['mp3', 'wav', 'alac', 'flac', 'aiff', 'avi', 'mp4', 'mov', 'flv', 'wmv', 'm4v', 'mpeg', 'zip', 'rar', 'torrent', 'tar', 'tar.gz', 'gz'];
				//$allowed_exts = ['php', 'html', 'htm', 'aspx', 'asp'];

				$bookmark_title = htmlspecialchars($_POST['bookmark_title']);
				$bookmark_url = $_POST['bookmark_url'];
				$bookmark_cat = $_POST['bookmark_cat'];

				$bookmark_url_ext = pathinfo($bookmark_url, PATHINFO_EXTENSION);

				if(!$bookmark_url_ext || (!in_array($bookmark_url_ext, $disallowed_exts) && !in_array($bookmark_url_ext, $direct_download_exts))) { //If there is no extension or the extension is in the allowed list.. continue

					if (!preg_match("~^(?:f|ht)tps?://~i", $bookmark_url)) {
				        $bookmark_url = "http://" . $bookmark_url;
				    }

				    if(empty($bookmark_title)) {

						$url = $bookmark_url;

						$ch = curl_init();

					    curl_setopt($ch, CURLOPT_HEADER, 0);
					    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.64 Safari/537.31');
					    curl_setopt($ch, CURLOPT_URL, $url);
					    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

					    $html = curl_exec($ch);
					    curl_close($ch);

					    if($html != '') {

							//parsing begins here:
							$doc = new DOMDocument();
							@$doc->loadHTML($html);
							$nodes = $doc->getElementsByTagName('title');

							//get and display what you need:
							$title = $nodes->item(0)->nodeValue;

							$bookmark_title = $title;
						
						} else {

							$unreachable = true;
						}
					}


					if($unreachable == true) {

						return 'url_unreachable';

					} else {

						$today = date("Y-m-d H:i:s");
						$date_added = date('d/m/Y');

						$query = $this->dbo->prepare("INSERT INTO bookmarks SET user_id = ?, title = ?, url = ?, cat = ?, date_added = ?, type = ?");
						$query->execute(array($user_id, $bookmark_title, $bookmark_url, $bookmark_cat, $today, 'page'));

						$bookmark_id = $this->dbo->lastInsertId();

						return '<section class="panel panel-default cat-' . $bookmark_cat . '" id="' . $bookmark_id . '" data-cat="'. $bookmark_cat .'">
			                                    <header class="panel-heading font-bold"><a href="' . $bookmark_url . '" target="_blank" class="bookmark_title" title="' . $bookmark_title . '">' . $bookmark_title . '</a></header>
			                                    <div class="panel-body">
			                                        <a href="' . $bookmark_url . '" target="_blank"><img src="images/placeholder.png" data-original="images/ss.png" /></a>

			                                        <div class="url"><a href="' . $bookmark_url . '" target="_blank" class="bookmark_url" title="' . $bookmark_url . '">' . $bookmark_url . '</a></div>
			                                    </div>
			                                    <footer class="panel-footer bg-white no-padder">
			                                        <div class="row text-center no-gutter">
			                                            <div class="col-xs-5 b-r b-light"><a href="' . $bookmark_url . '" target="_blank" class="h4 live_site">Live Site</a></div>
			                                            <div class="col-xs-5 b-r b-light"><a href="" target="_blank" class="h4 static_copy disabled" data-date="' . $date_added . '">Static Copy</a></div>
			                                            <div class="col-xs-2 b-r b-light">
			                                                <a href="#" class="h4 dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></a>
			                                                <ul class="dropdown-menu animated fadeInRight">
			                                                    <span class="arrow top"></span> 
			                                                    <li><a href="#" class="delete_bookmark"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete Bookmark</a></li>
			                                                </ul>
			                                            </div>
			                                        </div>
			                                    </footer>
			                                    <div class="loader"></div>
			                                </section>';

			                                //<li><a href="#" class="update_static_copy"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Update Static Copy</a></li>
			        }

		        } else if(in_array($bookmark_url_ext, $direct_download_exts)) { //Otherwise, if extension is a allowed direct download.... then download it straight.

		        	if (!preg_match("~^(?:f|ht)tps?://~i", $bookmark_url)) {
				        $bookmark_url = "http://" . $bookmark_url;
				    }

				    if(empty($bookmark_title)) {

						$bookmark_title = basename($bookmark_url);
					}

					$today = date("Y-m-d H:i:s");
					$date_added = date('d/m/Y');

					$query = $this->dbo->prepare("INSERT INTO bookmarks SET user_id = ?, title = ?, url = ?, cat = ?, date_added = ?, type = ?, ext = ?");
					$query->execute(array($user_id, $bookmark_title, $bookmark_url, $bookmark_cat, $today, 'file', $bookmark_url_ext));

					$bookmark_id = $this->dbo->lastInsertId();

		        	return '<section class="panel panel-default cat-' . $bookmark_cat . '" id="' . $bookmark_id . '" data-cat="'. $bookmark_cat .'">
		                                    <header class="panel-heading font-bold"><a href="' . $bookmark_url . '" target="_blank" class="bookmark_title" title="' . $bookmark_title . '">' . $bookmark_title . '</a></header>
		                                    <div class="panel-body">
		                                        <a href="' . $bookmark_url . '" target="_blank"><img src="images/placeholder.png" data-original="images/ss.png" /></a>

		                                        <div class="url"><a href="' . $bookmark_url . '" target="_blank" class="bookmark_url" title="' . $bookmark_url . '">' . $bookmark_url . '</a></div>
		                                    </div>
		                                    <footer class="panel-footer bg-white no-padder">
		                                        <div class="row text-center no-gutter">
		                                            <div class="col-xs-5 b-r b-light"><a href="' . $bookmark_url . '" target="_blank" class="h4 live_site">Live Site</a></div>
		                                            <div class="col-xs-5 b-r b-light"><a href="" target="_blank" class="h4 static_copy disabled" data-date="' . $date_added . '">Static Copy</a></div>
		                                            <div class="col-xs-2 b-r b-light">
		                                                <a href="#" class="h4 dropdown-toggle" data-toggle="dropdown"><i class="fa fa-angle-down"></i></a>
		                                                <ul class="dropdown-menu animated fadeInRight">
		                                                    <span class="arrow top"></span>
		                                                    <li><a href="#" class="delete_bookmark"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete Bookmark</a></li>
		                                                </ul>
		                                            </div>
		                                        </div>
		                                    </footer>
		                                    <div class="loader"></div>
		                                </section>';

		                                //<li><a href="#" class="update_static_copy"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Update Static Copy</a></li>
		        	
		        } else {

		        	return 'not_allowed';
		        }

			}



			public function updateBookmark() {

				$user_id = $_SESSION['user_id'];

				$bookmark_id = $_POST['bookmark_id'];
				$bookmark_title = $_POST['bookmark_title'];
				$bookmark_url = $_POST['bookmark_url'];
				$bookmark_cat = $_POST['bookmark_cat'];

				$query = $this->dbo->prepare("UPDATE bookmarks SET title = ?, url = ?, cat = ? WHERE user_id = ? AND id = ?");
				$query->execute(array($bookmark_title, $bookmark_url, $bookmark_cat, $user_id, $bookmark_id));

				return 'success';

			}


			public function deleteBookmark() {

				require_once("S3.class.php");
				$s3 = new S3();

				$user_id = $_SESSION['user_id'];
				$username = $_SESSION['username'];

				$bookmark_id = $_POST['bookmark_id'];

				$result = $s3->deleteObject($username, $bookmark_id);

				$query = $this->dbo->prepare("DELETE FROM bookmarks WHERE user_id = ? AND id = ?");
				$query->execute(array($user_id, $bookmark_id));

				return $result;

			}


			public function getStaticCopy() {

				require_once("S3.class.php");
				$s3 = new S3();

				$user_id = $_SESSION['user_id'];
				$username = $_SESSION['username'];

				$bookmark_id = $_POST['bookmark_id'];

				ignore_user_abort(true);
				set_time_limit(60);

				$query = $this->dbo->prepare("SELECT url, type, ext FROM bookmarks WHERE user_id = ? AND id = ?");
				$query->execute(array($user_id, $bookmark_id));

				$result = $query->fetchAll();

				foreach($result as $row) {

		    		$url = $row[0];
		    		$type = $row[1];
		    		$ext = $row[2];
		    	}

		    	$url_parsed = parse_url($url);
				$url_base = $url_parsed[host];


		    	if($type == 'page') {

		    		$directory_path = '/var/www/html/tmp_files/' . $username . '_' . $url_base; // used for deleting directory

					$file_path = '/var/www/html/tmp_files/' . $username . '_' . $url_base . '/static.html';

					$new_filename = $bookmark_id . '_' . $url_base . '.html';

					exec('httrack "' . $url . '" -O "/var/www/html/tmp_files/' . $username . '_' . $url_base . '" "+*.' . $url_base . '/*" -v -r1 -N "static.html"', $output);


					$static_url = $s3->putObject($username, $new_filename, $file_path, $bookmark_id);
					$static_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $static_url);

					chdir('../tmp_files');
					exec('phantomjs ../app/scripts/screenshot.js ' . $url . ' ' . $bookmark_id);

					$new_img = $bookmark_id . '_' . $url_base . '.png';
					$img_path = '/var/www/html/tmp_files/' . $bookmark_id . '_screenshot.png';

					exec('convert ' . $img_path . ' -resize 30% ' . $img_path);


					$img_url = $s3->putObject($username, $new_img, $img_path, $bookmark_id);
					$img_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $img_url);


					$query = $this->dbo->prepare("UPDATE bookmarks SET static_url = ?, thumbnail = ? WHERE user_id = ? AND id = ?");
					$query->execute(array($static_url, $img_url, $user_id, $bookmark_id));

					//Delete temp files
					exec('rm -rf ' . $directory_path);
					exec('rm -f ' . $img_path);

					return $img_url . ',' . $static_url;

				} else if($type == 'file') {

					file_put_contents("../tmp_files/" . $username . '_' . basename($url), fopen($url, 'r'));

					$file_path = '/var/www/html/tmp_files/' . $username . '_' . basename($url);

					$new_filename = $bookmark_id . '_' . basename($url);


					$img_ext_array = ['png', 'jpg', 'jpeg', 'gif'];
					if(in_array($ext, $img_ext_array)) {

						$new_img = $bookmark_id . '_' . basename($url);
						$new_img_thumb = 'thumb_' . $bookmark_id . '_' . basename($url);
						$img_path = '/var/www/html/tmp_files/' . $username . '_' . basename($url);
						$img_path_resized = '/var/www/html/tmp_files/thumb_' . $username . '_' . basename($url);

						exec('convert ' . $img_path . ' -resize 320x500 -gravity center -extent 320x215 ' . $img_path_resized);


						//Thumbnail
						$resized_img_url = $s3->putObject($username, $new_img_thumb, $img_path_resized, $bookmark_id);
						$resized_img_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $resized_img_url);

						//Fullsize
						$fullsize_img_url = $s3->putObject($username, $new_img, $img_path, $bookmark_id);
						$fullsize_img_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $fullsize_img_url);

						$query = $this->dbo->prepare("UPDATE bookmarks SET static_url = ?, thumbnail = ? WHERE user_id = ? AND id = ?");
						$query->execute(array($fullsize_img_url, $resized_img_url, $user_id, $bookmark_id));

						//Delete temp files
						exec('rm -f ' . $img_path);
						exec('rm -f ' . $img_path_resized);

						return $resized_img_url . ',' . $fullsize_img_url;
					
					} else {

						$img_url = "images/file_placeholder.png";

						$static_url = $s3->putObject($username, $new_filename, $file_path, $bookmark_id);
						$static_url = str_replace('https://evermark1.s3.amazonaws.com/', 'http://d2rw30husg8b1z.cloudfront.net/', $static_url);

						$query = $this->dbo->prepare("UPDATE bookmarks SET static_url = ?, thumbnail = ? WHERE user_id = ? AND id = ?");
						$query->execute(array($static_url, $img_url, $user_id, $bookmark_id));

						//Delete temp files
						exec('rm -f ' . $file_path);

						return $img_url . ',' . $static_url;
					}

				}

			}

	}
?>