<?php

    require_once('functions.php');

    $page_html = load_page_html();

    if($_GET['e'] == 'email_exists') {

        $message = '<span class="login_message error">The email address is already in use. You may have previously signed up with us!</span>';
    
    } else if($_GET['e'] == 'insecure_password') {

        $message = '<span class="login_message error">Insecure Password! Please choose a password with at least 8 characters.</span>';
    
    } else if($_GET['e'] == "username_exists") {

        $message = '<span class="login_message error">The username is already in use. Please use another one.</span>';
    
    } else if($_GET['e'] == "username_too_short") {

        $message = '<span class="login_message error">The username is too short. Please choose one with at least 3 characters.</span>';
    }

?>
<!DOCTYPE html>
<html lang="en" class="bg-dark">

    <?php require_once("inc/head.php"); ?>

    <body>
        <section id="content" class="m-t-lg wrapper-md animated fadeInDown">
            <div class="container aside-xxl">
                <a class="navbar-brand block" href="../index.php"><img src="images/logo.png" "Evermark" /></a> 
                <section class="panel panel-default m-t-lg bg-white">
                    <header class="panel-heading text-center"> <strong>Sign up</strong> </header>
                    <div class="login_message_wrapper"><?php echo $message; ?></div>
                    <form action="signup.php?action=signup" method="post" class="panel-body wrapper-lg signup_form">
                        <div class="form-group"> <label class="control-label">Username</label> <input type="text" id="username" name="username" placeholder="Username (Minimum 3 Chars.)" class="form-control"> <span class="username_error"></span> </div>
                        <div class="form-group"> <label class="control-label">Email</label> <input type="email" id="email" name="email" placeholder="Email" class="form-control"> </div>
                        <div class="form-group"> <label class="control-label">Password</label> <input type="password" id="password" name="password" placeholder="Password (Minimum 8 Chars.)" class="form-control"> </div>
                        <!-- <div class="checkbox"> <label> <input type="checkbox" name="terms_of_use"> I have read and agree to the <a href="../terms_of_use.php" target="_blank">Terms of Use</a></label></div> -->
                        <input type="submit" class="btn btn-primary" value="Sign up" /> 
                        <div class="line line-dashed"></div>
                        <p class="text-muted text-center"><small>Already have an account?</small></p>
                        <a href="login.php" class="btn btn-primary btn-block">Sign in</a> 
                    </form>
                </section>
            </div>
        </section>
        <!-- footer --> 
        <footer id="footer">
            <div class="text-center padder clearfix">
                <p> <small>&copy; Copyright <?php echo date('Y'); ?> Evermark.</small> </p>
            </div>
        </footer>
        
        <?php require_once("inc/footer.php"); ?>
    </body>
</html>