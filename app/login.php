<?php

    require_once('functions.php');

    $page_html = load_page_html();

    if($_GET['e'] == 'login_failed') {

        $message = '<span class="login_message error">Wrong username and/or password. Please double-check your credentials and try again.</span>';
    
    } else if($_GET['e'] == 'verification_failed') {

        $message = '<span class="login_message error">We failed to verify your email address. Please try again.</span>';
    
    } else if($_GET['m'] == 'verified') {

        $message = '<span class="login_message success">Your email address has been verified. Please proceed to login.</span>';
        
    } else if($_GET['m'] == 'confirm_email') {

        $message = '<span class="login_message success">Please verify your email! We have sent you an email with a verification link.</span>';
    
    } else if($_GET['e'] == 'verification_required') {

        $message = '<span class="login_message error">To login you first need to verify your account. We have sent you an email with a verification link.</span>';
    
    }  else if($_GET['m'] == 'password_changed') {

        $message = '<span class="login_message success">Your password has been changed. Please proceed to login.</span>';
    
    }

?>

<!DOCTYPE html>
<html lang="en" class="bg-dark">

    <?php require_once("inc/head.php"); ?>

    <body>
        <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
            <div class="container aside-xxl">
                <a class="navbar-brand block" href="../index.php"><img src="images/logo.png" "Evermark" /></a> 
                <section class="panel panel-default bg-white m-t-lg">
                    <header class="panel-heading text-center"> <strong>Sign in</strong> </header>
                    <div class="login_message_wrapper"><?php echo $message; ?></div>
                    <form action="login.php?action=login" method="post" class="panel-body wrapper-lg">
                        <div class="form-group"> <label class="control-label">Username</label> <input type="text" name="username" placeholder="username" class="form-control"> </div>
                        <div class="form-group"> <label class="control-label">Password</label> <input type="password" name="password" placeholder="password" class="form-control"> </div>
                        <!-- <div class="checkbox"> <label> <input type="checkbox"> Keep me logged in </label> </div> -->
                        <a href="forgot_password.php" class="pull-right m-t-xs"><small>Forgot password?</small></a> 
                        <input type="submit" class="btn btn-primary" value="Sign in" /> 
                        <div class="line line-dashed"></div>
                        <!-- <a href="#" class="btn btn-facebook btn-block m-b-sm"><i class="fa fa-facebook pull-left"></i>Sign in with Facebook</a> <a href="#" class="btn btn-twitter btn-block"><i class="fa fa-twitter pull-left"></i>Sign in with Twitter</a> 
                        <div class="line line-dashed"></div> -->
                        <p class="text-muted text-center"><small>Don't have an account?</small></p>
                        <a href="signup.php" class="btn btn-primary btn-block">Create an account</a> 
                    </form>
                </section>
            </div>
        </section>
        <!-- footer --> 
        <footer id="footer">
            <div class="text-center padder">
                <p> <small>&copy; Copyright <?php echo date('Y'); ?> Evermark.</small> </p>
            </div>
        </footer>
        <!-- / footer --> <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
    </body>
</html>