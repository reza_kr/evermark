<?php

    require_once('functions.php');

    $page_html = load_page_html();

    if($_GET['e'] == 'password_mismatch') {

        $message = '<span class="login_message error">The password you entered did not match with the confirmation. Please try again.</span>';
    
    } else if($_GET['e'] == 'insecure_password') {

        $message = '<span class="login_message error">Insecure Password! Please choose a password with at least 8 characters.</span>';
    }

?>
<!DOCTYPE html>
<html lang="en" class="bg-dark">

    <?php require_once("inc/head.php"); ?>

    <body>
        <section id="content" class="m-t-lg wrapper-md animated fadeInUp">
            <div class="container aside-xxl">
                <a class="navbar-brand block" href="../index.php"><img src="images/logo.png" "Evermark" /></a> 
                <section class="panel panel-default bg-white m-t-lg">
                    <header class="panel-heading text-center"> <strong>Password Change</strong> </header>
                    <div class="login_message_wrapper"><?php echo $message; ?></div>
                    <form action="password_change.php?action=change_password" method="post" class="panel-body wrapper-lg">
                        <div class="form-group"> <label class="control-label">New Password</label> <input type="password" name="password1" class="form-control"> </div>
                        <div class="form-group"> <label class="control-label">Confirm Password</label> <input type="password" name="password2" class="form-control"> </div>
                        
                        <input type="hidden" name="email" value="<?php echo $_GET['email'];?>" />
                        <input type="hidden" name="token" value="<?php echo $_GET['token'];?>" />
                        <input type="submit" class="btn btn-primary" value="Change Password" /> 
                        <div class="line line-dashed"></div>

                        <p class="text-muted text-center"><small>Remember your credentials?</small></p>
                        <a href="login.php" class="btn btn-primary btn-block">Sign in</a> 
                    </form>
                </section>
            </div>
        </section>
        <!-- footer --> 
        <footer id="footer">
            <div class="text-center padder">
                <p> <small>&copy; Copyright <?php echo date('Y'); ?> Evermark.</small> </p>
            </div>
        </footer>
        <!-- / footer --> <script src="js/app.v2.js"></script> <!-- Bootstrap --> <!-- App --> 
    </body>
</html>