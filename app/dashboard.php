<?php
    
    require_once('functions.php');
    $page_html = load_page_html();

?>
<!DOCTYPE html>
<html lang="en" class="app">
    
	<?php require_once("inc/head.php"); ?>

    <body>
        <div class="show_on_mobile">
                
                <h1>Thank your for using Evermark!</h1>

                <h3>Our mobile and tablet apps are coming soon! For now you can access Evermark on your desktop.</h3>

        </div>
    
        <section class="vbox">

        	<?php require_once("inc/header.php"); ?>
            
            <section>
                <section class="hbox stretch">
                    <!-- .aside --> 
                    
                    <?php require_once("inc/nav.php"); ?>

                    <!-- /.aside --> 
                    <section id="content">
                        <section class="vbox">

                            <div class="add_bookmark_wrapper">
                                <form method="post" action="#">
                                    <input type="text" id="bookmark_title" placeholder="Title (Optional)" autocomplete="off" />
                                    <input type="text" id="bookmark_url" placeholder="URL" autocomplete="off" />
                                    <select id="category">
                                        
                                    </select>

                                    <input type="submit" id="save_bookmark" value="Add Bookmark" />

                                    <div class="spinner"></div>

                                    <!-- <a href="#" class="close_add_bookmark"><i class="fa fa-times"></i></a> -->
                                </form>
                            </div>

                            <!-- <div class="edit_bookmark_wrapper">
                                <form method="post" action="#">
                                    <input type="text" id="bookmark_title" placeholder="Title" autocomplete="off" />
                                    <input type="text" id="bookmark_url" placeholder="URL" autocomplete="off" />
                                    <select id="category">
                                        
                                    </select>

                                    <input type="hidden" id="bookmark_id" />
                                    <input type="submit" id="update_bookmark" value="Update Bookmark" />

                                    <div class="spinner"></div>

                                    <a href="#" class="close_edit_bookmark"><i class="fa fa-times"></i></a>
                                </form>
                            </div> -->

                            <section class=" padder isotope">

                                <div class="no_results">Search Returned No Results.</div>

                                <?php echo $page_html['bookmarks']; ?>

                            </section>

                            <div class="static_viewer">
                                <div class="static_viewer_header">
                                    <span class="page_title">Evermark | Bookmarks that last forever and ever and ever</span>
                                    <a href="#" class="close_static_viewer"><i class="fa fa-times"></i></a>
                                </div>
                                <iframe id="static_viewer_frame" src=""></iframe>
                            </div>

                            <nav id="pagination">

                                <?php 

                                    //$page = $_GET['page'];

                                    //echo '<a href="dashboard.php?page=' . ($page + 1) . '">' . ($page + 1) . '</a>';

                                ?>

                            </nav>

                            <div id="not_allowed">
                                The requested file format is not allowed.
                            </div>

                            <div id="url_unreachable">
                                The specified URL is unreachable.
                            </div>

                        </section>

                    </section>
                </section>
            </section>
        </section>
        
        <?php require_once("inc/footer.php"); ?>

    </body>
</html>