
// var page = require('webpage').create();  
// var fs = require('fs');// File System Module

// var url = "http://google.com"; //args[1];

// page.open("http://google.com");
// page.onLoadFinished = function() {

//   page.render('github.png');
//   phantom.exit();

// };

var args = require('system').args;
var url = args[1];
var bookmark_id = args[2];

var page = require('webpage').create();
page.viewportSize = {width: 1100, height: 737};
page.clipRect = {top: 0, left: 0, width: 1100, height: 737};

page.open(url , function () {
    page.render(bookmark_id + '_screenshot.png');
    phantom.exit();
});